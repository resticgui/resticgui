// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"
	"time"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/resticgui/resticgui/ent/predicate"
	"gitlab.com/resticgui/resticgui/ent/user"
)

// UserUpdate is the builder for updating User entities.
type UserUpdate struct {
	config
	hooks    []Hook
	mutation *UserMutation
}

// Where appends a list predicates to the UserUpdate builder.
func (uu *UserUpdate) Where(ps ...predicate.User) *UserUpdate {
	uu.mutation.Where(ps...)
	return uu
}

// SetUsername sets the "username" field.
func (uu *UserUpdate) SetUsername(s string) *UserUpdate {
	uu.mutation.SetUsername(s)
	return uu
}

// SetEmail sets the "email" field.
func (uu *UserUpdate) SetEmail(s string) *UserUpdate {
	uu.mutation.SetEmail(s)
	return uu
}

// SetPassword sets the "password" field.
func (uu *UserUpdate) SetPassword(s string) *UserUpdate {
	uu.mutation.SetPassword(s)
	return uu
}

// SetLastConnectionDate sets the "LastConnectionDate" field.
func (uu *UserUpdate) SetLastConnectionDate(t time.Time) *UserUpdate {
	uu.mutation.SetLastConnectionDate(t)
	return uu
}

// SetNillableLastConnectionDate sets the "LastConnectionDate" field if the given value is not nil.
func (uu *UserUpdate) SetNillableLastConnectionDate(t *time.Time) *UserUpdate {
	if t != nil {
		uu.SetLastConnectionDate(*t)
	}
	return uu
}

// ClearLastConnectionDate clears the value of the "LastConnectionDate" field.
func (uu *UserUpdate) ClearLastConnectionDate() *UserUpdate {
	uu.mutation.ClearLastConnectionDate()
	return uu
}

// SetPasswordLastSet sets the "PasswordLastSet" field.
func (uu *UserUpdate) SetPasswordLastSet(t time.Time) *UserUpdate {
	uu.mutation.SetPasswordLastSet(t)
	return uu
}

// SetNillablePasswordLastSet sets the "PasswordLastSet" field if the given value is not nil.
func (uu *UserUpdate) SetNillablePasswordLastSet(t *time.Time) *UserUpdate {
	if t != nil {
		uu.SetPasswordLastSet(*t)
	}
	return uu
}

// ClearPasswordLastSet clears the value of the "PasswordLastSet" field.
func (uu *UserUpdate) ClearPasswordLastSet() *UserUpdate {
	uu.mutation.ClearPasswordLastSet()
	return uu
}

// SetPasswordFailCount sets the "PasswordFailCount" field.
func (uu *UserUpdate) SetPasswordFailCount(i int) *UserUpdate {
	uu.mutation.ResetPasswordFailCount()
	uu.mutation.SetPasswordFailCount(i)
	return uu
}

// SetNillablePasswordFailCount sets the "PasswordFailCount" field if the given value is not nil.
func (uu *UserUpdate) SetNillablePasswordFailCount(i *int) *UserUpdate {
	if i != nil {
		uu.SetPasswordFailCount(*i)
	}
	return uu
}

// AddPasswordFailCount adds i to the "PasswordFailCount" field.
func (uu *UserUpdate) AddPasswordFailCount(i int) *UserUpdate {
	uu.mutation.AddPasswordFailCount(i)
	return uu
}

// SetMfa sets the "mfa" field.
func (uu *UserUpdate) SetMfa(b bool) *UserUpdate {
	uu.mutation.SetMfa(b)
	return uu
}

// SetNillableMfa sets the "mfa" field if the given value is not nil.
func (uu *UserUpdate) SetNillableMfa(b *bool) *UserUpdate {
	if b != nil {
		uu.SetMfa(*b)
	}
	return uu
}

// Mutation returns the UserMutation object of the builder.
func (uu *UserUpdate) Mutation() *UserMutation {
	return uu.mutation
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (uu *UserUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(uu.hooks) == 0 {
		affected, err = uu.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*UserMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			uu.mutation = mutation
			affected, err = uu.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(uu.hooks) - 1; i >= 0; i-- {
			if uu.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = uu.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, uu.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (uu *UserUpdate) SaveX(ctx context.Context) int {
	affected, err := uu.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (uu *UserUpdate) Exec(ctx context.Context) error {
	_, err := uu.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (uu *UserUpdate) ExecX(ctx context.Context) {
	if err := uu.Exec(ctx); err != nil {
		panic(err)
	}
}

func (uu *UserUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   user.Table,
			Columns: user.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: user.FieldID,
			},
		},
	}
	if ps := uu.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := uu.mutation.Username(); ok {
		_spec.SetField(user.FieldUsername, field.TypeString, value)
	}
	if value, ok := uu.mutation.Email(); ok {
		_spec.SetField(user.FieldEmail, field.TypeString, value)
	}
	if value, ok := uu.mutation.Password(); ok {
		_spec.SetField(user.FieldPassword, field.TypeString, value)
	}
	if value, ok := uu.mutation.LastConnectionDate(); ok {
		_spec.SetField(user.FieldLastConnectionDate, field.TypeTime, value)
	}
	if uu.mutation.LastConnectionDateCleared() {
		_spec.ClearField(user.FieldLastConnectionDate, field.TypeTime)
	}
	if value, ok := uu.mutation.PasswordLastSet(); ok {
		_spec.SetField(user.FieldPasswordLastSet, field.TypeTime, value)
	}
	if uu.mutation.PasswordLastSetCleared() {
		_spec.ClearField(user.FieldPasswordLastSet, field.TypeTime)
	}
	if value, ok := uu.mutation.PasswordFailCount(); ok {
		_spec.SetField(user.FieldPasswordFailCount, field.TypeInt, value)
	}
	if value, ok := uu.mutation.AddedPasswordFailCount(); ok {
		_spec.AddField(user.FieldPasswordFailCount, field.TypeInt, value)
	}
	if value, ok := uu.mutation.Mfa(); ok {
		_spec.SetField(user.FieldMfa, field.TypeBool, value)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, uu.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{user.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	return n, nil
}

// UserUpdateOne is the builder for updating a single User entity.
type UserUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *UserMutation
}

// SetUsername sets the "username" field.
func (uuo *UserUpdateOne) SetUsername(s string) *UserUpdateOne {
	uuo.mutation.SetUsername(s)
	return uuo
}

// SetEmail sets the "email" field.
func (uuo *UserUpdateOne) SetEmail(s string) *UserUpdateOne {
	uuo.mutation.SetEmail(s)
	return uuo
}

// SetPassword sets the "password" field.
func (uuo *UserUpdateOne) SetPassword(s string) *UserUpdateOne {
	uuo.mutation.SetPassword(s)
	return uuo
}

// SetLastConnectionDate sets the "LastConnectionDate" field.
func (uuo *UserUpdateOne) SetLastConnectionDate(t time.Time) *UserUpdateOne {
	uuo.mutation.SetLastConnectionDate(t)
	return uuo
}

// SetNillableLastConnectionDate sets the "LastConnectionDate" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableLastConnectionDate(t *time.Time) *UserUpdateOne {
	if t != nil {
		uuo.SetLastConnectionDate(*t)
	}
	return uuo
}

// ClearLastConnectionDate clears the value of the "LastConnectionDate" field.
func (uuo *UserUpdateOne) ClearLastConnectionDate() *UserUpdateOne {
	uuo.mutation.ClearLastConnectionDate()
	return uuo
}

// SetPasswordLastSet sets the "PasswordLastSet" field.
func (uuo *UserUpdateOne) SetPasswordLastSet(t time.Time) *UserUpdateOne {
	uuo.mutation.SetPasswordLastSet(t)
	return uuo
}

// SetNillablePasswordLastSet sets the "PasswordLastSet" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillablePasswordLastSet(t *time.Time) *UserUpdateOne {
	if t != nil {
		uuo.SetPasswordLastSet(*t)
	}
	return uuo
}

// ClearPasswordLastSet clears the value of the "PasswordLastSet" field.
func (uuo *UserUpdateOne) ClearPasswordLastSet() *UserUpdateOne {
	uuo.mutation.ClearPasswordLastSet()
	return uuo
}

// SetPasswordFailCount sets the "PasswordFailCount" field.
func (uuo *UserUpdateOne) SetPasswordFailCount(i int) *UserUpdateOne {
	uuo.mutation.ResetPasswordFailCount()
	uuo.mutation.SetPasswordFailCount(i)
	return uuo
}

// SetNillablePasswordFailCount sets the "PasswordFailCount" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillablePasswordFailCount(i *int) *UserUpdateOne {
	if i != nil {
		uuo.SetPasswordFailCount(*i)
	}
	return uuo
}

// AddPasswordFailCount adds i to the "PasswordFailCount" field.
func (uuo *UserUpdateOne) AddPasswordFailCount(i int) *UserUpdateOne {
	uuo.mutation.AddPasswordFailCount(i)
	return uuo
}

// SetMfa sets the "mfa" field.
func (uuo *UserUpdateOne) SetMfa(b bool) *UserUpdateOne {
	uuo.mutation.SetMfa(b)
	return uuo
}

// SetNillableMfa sets the "mfa" field if the given value is not nil.
func (uuo *UserUpdateOne) SetNillableMfa(b *bool) *UserUpdateOne {
	if b != nil {
		uuo.SetMfa(*b)
	}
	return uuo
}

// Mutation returns the UserMutation object of the builder.
func (uuo *UserUpdateOne) Mutation() *UserMutation {
	return uuo.mutation
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (uuo *UserUpdateOne) Select(field string, fields ...string) *UserUpdateOne {
	uuo.fields = append([]string{field}, fields...)
	return uuo
}

// Save executes the query and returns the updated User entity.
func (uuo *UserUpdateOne) Save(ctx context.Context) (*User, error) {
	var (
		err  error
		node *User
	)
	if len(uuo.hooks) == 0 {
		node, err = uuo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*UserMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			uuo.mutation = mutation
			node, err = uuo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(uuo.hooks) - 1; i >= 0; i-- {
			if uuo.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = uuo.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, uuo.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*User)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from UserMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (uuo *UserUpdateOne) SaveX(ctx context.Context) *User {
	node, err := uuo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (uuo *UserUpdateOne) Exec(ctx context.Context) error {
	_, err := uuo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (uuo *UserUpdateOne) ExecX(ctx context.Context) {
	if err := uuo.Exec(ctx); err != nil {
		panic(err)
	}
}

func (uuo *UserUpdateOne) sqlSave(ctx context.Context) (_node *User, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   user.Table,
			Columns: user.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: user.FieldID,
			},
		},
	}
	id, ok := uuo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "User.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := uuo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, user.FieldID)
		for _, f := range fields {
			if !user.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != user.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := uuo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := uuo.mutation.Username(); ok {
		_spec.SetField(user.FieldUsername, field.TypeString, value)
	}
	if value, ok := uuo.mutation.Email(); ok {
		_spec.SetField(user.FieldEmail, field.TypeString, value)
	}
	if value, ok := uuo.mutation.Password(); ok {
		_spec.SetField(user.FieldPassword, field.TypeString, value)
	}
	if value, ok := uuo.mutation.LastConnectionDate(); ok {
		_spec.SetField(user.FieldLastConnectionDate, field.TypeTime, value)
	}
	if uuo.mutation.LastConnectionDateCleared() {
		_spec.ClearField(user.FieldLastConnectionDate, field.TypeTime)
	}
	if value, ok := uuo.mutation.PasswordLastSet(); ok {
		_spec.SetField(user.FieldPasswordLastSet, field.TypeTime, value)
	}
	if uuo.mutation.PasswordLastSetCleared() {
		_spec.ClearField(user.FieldPasswordLastSet, field.TypeTime)
	}
	if value, ok := uuo.mutation.PasswordFailCount(); ok {
		_spec.SetField(user.FieldPasswordFailCount, field.TypeInt, value)
	}
	if value, ok := uuo.mutation.AddedPasswordFailCount(); ok {
		_spec.AddField(user.FieldPasswordFailCount, field.TypeInt, value)
	}
	if value, ok := uuo.mutation.Mfa(); ok {
		_spec.SetField(user.FieldMfa, field.TypeBool, value)
	}
	_node = &User{config: uuo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, uuo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{user.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	return _node, nil
}
