// Code generated by ent, DO NOT EDIT.

package backuppolicy

const (
	// Label holds the string label denoting the backuppolicy type in the database.
	Label = "backup_policy"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldNbKeepLast holds the string denoting the nbkeeplast field in the database.
	FieldNbKeepLast = "nb_keep_last"
	// FieldNbKeepHours holds the string denoting the nbkeephours field in the database.
	FieldNbKeepHours = "nb_keep_hours"
	// FieldNbKeepDays holds the string denoting the nbkeepdays field in the database.
	FieldNbKeepDays = "nb_keep_days"
	// FieldNbKeepWeeks holds the string denoting the nbkeepweeks field in the database.
	FieldNbKeepWeeks = "nb_keep_weeks"
	// FieldNbKeepMonths holds the string denoting the nbkeepmonths field in the database.
	FieldNbKeepMonths = "nb_keep_months"
	// FieldKeepAll holds the string denoting the keepall field in the database.
	FieldKeepAll = "keep_all"
	// EdgeRepository holds the string denoting the repository edge name in mutations.
	EdgeRepository = "repository"
	// Table holds the table name of the backuppolicy in the database.
	Table = "BackupPolicy"
	// RepositoryTable is the table that holds the repository relation/edge.
	RepositoryTable = "BackupPolicy"
	// RepositoryInverseTable is the table name for the Repository entity.
	// It exists in this package in order to avoid circular dependency with the "repository" package.
	RepositoryInverseTable = "repositories"
	// RepositoryColumn is the table column denoting the repository relation/edge.
	RepositoryColumn = "repository_backup_policy"
)

// Columns holds all SQL columns for backuppolicy fields.
var Columns = []string{
	FieldID,
	FieldNbKeepLast,
	FieldNbKeepHours,
	FieldNbKeepDays,
	FieldNbKeepWeeks,
	FieldNbKeepMonths,
	FieldKeepAll,
}

// ForeignKeys holds the SQL foreign-keys that are owned by the "BackupPolicy"
// table and are not defined as standalone fields in the schema.
var ForeignKeys = []string{
	"repository_backup_policy",
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	for i := range ForeignKeys {
		if column == ForeignKeys[i] {
			return true
		}
	}
	return false
}

var (
	// DefaultNbKeepLast holds the default value on creation for the "nbKeepLast" field.
	DefaultNbKeepLast uint
	// DefaultNbKeepHours holds the default value on creation for the "nbKeepHours" field.
	DefaultNbKeepHours uint
	// DefaultNbKeepDays holds the default value on creation for the "nbKeepDays" field.
	DefaultNbKeepDays uint
	// DefaultNbKeepWeeks holds the default value on creation for the "nbKeepWeeks" field.
	DefaultNbKeepWeeks uint
	// DefaultNbKeepMonths holds the default value on creation for the "nbKeepMonths" field.
	DefaultNbKeepMonths uint
	// DefaultKeepAll holds the default value on creation for the "keepAll" field.
	DefaultKeepAll bool
)
