// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/resticgui/resticgui/ent/backupschedule"
	"gitlab.com/resticgui/resticgui/ent/predicate"
)

// BackupScheduleDelete is the builder for deleting a BackupSchedule entity.
type BackupScheduleDelete struct {
	config
	hooks    []Hook
	mutation *BackupScheduleMutation
}

// Where appends a list predicates to the BackupScheduleDelete builder.
func (bsd *BackupScheduleDelete) Where(ps ...predicate.BackupSchedule) *BackupScheduleDelete {
	bsd.mutation.Where(ps...)
	return bsd
}

// Exec executes the deletion query and returns how many vertices were deleted.
func (bsd *BackupScheduleDelete) Exec(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(bsd.hooks) == 0 {
		affected, err = bsd.sqlExec(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*BackupScheduleMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			bsd.mutation = mutation
			affected, err = bsd.sqlExec(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(bsd.hooks) - 1; i >= 0; i-- {
			if bsd.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = bsd.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, bsd.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// ExecX is like Exec, but panics if an error occurs.
func (bsd *BackupScheduleDelete) ExecX(ctx context.Context) int {
	n, err := bsd.Exec(ctx)
	if err != nil {
		panic(err)
	}
	return n
}

func (bsd *BackupScheduleDelete) sqlExec(ctx context.Context) (int, error) {
	_spec := &sqlgraph.DeleteSpec{
		Node: &sqlgraph.NodeSpec{
			Table: backupschedule.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: backupschedule.FieldID,
			},
		},
	}
	if ps := bsd.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	affected, err := sqlgraph.DeleteNodes(ctx, bsd.driver, _spec)
	if err != nil && sqlgraph.IsConstraintError(err) {
		err = &ConstraintError{msg: err.Error(), wrap: err}
	}
	return affected, err
}

// BackupScheduleDeleteOne is the builder for deleting a single BackupSchedule entity.
type BackupScheduleDeleteOne struct {
	bsd *BackupScheduleDelete
}

// Exec executes the deletion query.
func (bsdo *BackupScheduleDeleteOne) Exec(ctx context.Context) error {
	n, err := bsdo.bsd.Exec(ctx)
	switch {
	case err != nil:
		return err
	case n == 0:
		return &NotFoundError{backupschedule.Label}
	default:
		return nil
	}
}

// ExecX is like Exec, but panics if an error occurs.
func (bsdo *BackupScheduleDeleteOne) ExecX(ctx context.Context) {
	bsdo.bsd.ExecX(ctx)
}
