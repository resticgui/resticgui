// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/resticgui/resticgui/ent/awss3provider"
	"gitlab.com/resticgui/resticgui/ent/predicate"
	"gitlab.com/resticgui/resticgui/ent/repository"
)

// AWSS3ProviderUpdate is the builder for updating AWSS3Provider entities.
type AWSS3ProviderUpdate struct {
	config
	hooks    []Hook
	mutation *AWSS3ProviderMutation
}

// Where appends a list predicates to the AWSS3ProviderUpdate builder.
func (au *AWSS3ProviderUpdate) Where(ps ...predicate.AWSS3Provider) *AWSS3ProviderUpdate {
	au.mutation.Where(ps...)
	return au
}

// SetBucketName sets the "bucketName" field.
func (au *AWSS3ProviderUpdate) SetBucketName(s string) *AWSS3ProviderUpdate {
	au.mutation.SetBucketName(s)
	return au
}

// SetSecretAccessKey sets the "secretAccessKey" field.
func (au *AWSS3ProviderUpdate) SetSecretAccessKey(s string) *AWSS3ProviderUpdate {
	au.mutation.SetSecretAccessKey(s)
	return au
}

// SetAccessKeyID sets the "accessKeyID" field.
func (au *AWSS3ProviderUpdate) SetAccessKeyID(s string) *AWSS3ProviderUpdate {
	au.mutation.SetAccessKeyID(s)
	return au
}

// SetDefaultRegion sets the "defaultRegion" field.
func (au *AWSS3ProviderUpdate) SetDefaultRegion(s string) *AWSS3ProviderUpdate {
	au.mutation.SetDefaultRegion(s)
	return au
}

// SetNillableDefaultRegion sets the "defaultRegion" field if the given value is not nil.
func (au *AWSS3ProviderUpdate) SetNillableDefaultRegion(s *string) *AWSS3ProviderUpdate {
	if s != nil {
		au.SetDefaultRegion(*s)
	}
	return au
}

// ClearDefaultRegion clears the value of the "defaultRegion" field.
func (au *AWSS3ProviderUpdate) ClearDefaultRegion() *AWSS3ProviderUpdate {
	au.mutation.ClearDefaultRegion()
	return au
}

// SetValid sets the "valid" field.
func (au *AWSS3ProviderUpdate) SetValid(b bool) *AWSS3ProviderUpdate {
	au.mutation.SetValid(b)
	return au
}

// SetNillableValid sets the "valid" field if the given value is not nil.
func (au *AWSS3ProviderUpdate) SetNillableValid(b *bool) *AWSS3ProviderUpdate {
	if b != nil {
		au.SetValid(*b)
	}
	return au
}

// SetRepositoryID sets the "repository" edge to the Repository entity by ID.
func (au *AWSS3ProviderUpdate) SetRepositoryID(id int) *AWSS3ProviderUpdate {
	au.mutation.SetRepositoryID(id)
	return au
}

// SetRepository sets the "repository" edge to the Repository entity.
func (au *AWSS3ProviderUpdate) SetRepository(r *Repository) *AWSS3ProviderUpdate {
	return au.SetRepositoryID(r.ID)
}

// Mutation returns the AWSS3ProviderMutation object of the builder.
func (au *AWSS3ProviderUpdate) Mutation() *AWSS3ProviderMutation {
	return au.mutation
}

// ClearRepository clears the "repository" edge to the Repository entity.
func (au *AWSS3ProviderUpdate) ClearRepository() *AWSS3ProviderUpdate {
	au.mutation.ClearRepository()
	return au
}

// Save executes the query and returns the number of nodes affected by the update operation.
func (au *AWSS3ProviderUpdate) Save(ctx context.Context) (int, error) {
	var (
		err      error
		affected int
	)
	if len(au.hooks) == 0 {
		if err = au.check(); err != nil {
			return 0, err
		}
		affected, err = au.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*AWSS3ProviderMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = au.check(); err != nil {
				return 0, err
			}
			au.mutation = mutation
			affected, err = au.sqlSave(ctx)
			mutation.done = true
			return affected, err
		})
		for i := len(au.hooks) - 1; i >= 0; i-- {
			if au.hooks[i] == nil {
				return 0, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = au.hooks[i](mut)
		}
		if _, err := mut.Mutate(ctx, au.mutation); err != nil {
			return 0, err
		}
	}
	return affected, err
}

// SaveX is like Save, but panics if an error occurs.
func (au *AWSS3ProviderUpdate) SaveX(ctx context.Context) int {
	affected, err := au.Save(ctx)
	if err != nil {
		panic(err)
	}
	return affected
}

// Exec executes the query.
func (au *AWSS3ProviderUpdate) Exec(ctx context.Context) error {
	_, err := au.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (au *AWSS3ProviderUpdate) ExecX(ctx context.Context) {
	if err := au.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (au *AWSS3ProviderUpdate) check() error {
	if v, ok := au.mutation.BucketName(); ok {
		if err := awss3provider.BucketNameValidator(v); err != nil {
			return &ValidationError{Name: "bucketName", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.bucketName": %w`, err)}
		}
	}
	if v, ok := au.mutation.SecretAccessKey(); ok {
		if err := awss3provider.SecretAccessKeyValidator(v); err != nil {
			return &ValidationError{Name: "secretAccessKey", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.secretAccessKey": %w`, err)}
		}
	}
	if v, ok := au.mutation.AccessKeyID(); ok {
		if err := awss3provider.AccessKeyIDValidator(v); err != nil {
			return &ValidationError{Name: "accessKeyID", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.accessKeyID": %w`, err)}
		}
	}
	if _, ok := au.mutation.RepositoryID(); au.mutation.RepositoryCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "AWSS3Provider.repository"`)
	}
	return nil
}

func (au *AWSS3ProviderUpdate) sqlSave(ctx context.Context) (n int, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   awss3provider.Table,
			Columns: awss3provider.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: awss3provider.FieldID,
			},
		},
	}
	if ps := au.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := au.mutation.BucketName(); ok {
		_spec.SetField(awss3provider.FieldBucketName, field.TypeString, value)
	}
	if value, ok := au.mutation.SecretAccessKey(); ok {
		_spec.SetField(awss3provider.FieldSecretAccessKey, field.TypeString, value)
	}
	if value, ok := au.mutation.AccessKeyID(); ok {
		_spec.SetField(awss3provider.FieldAccessKeyID, field.TypeString, value)
	}
	if value, ok := au.mutation.DefaultRegion(); ok {
		_spec.SetField(awss3provider.FieldDefaultRegion, field.TypeString, value)
	}
	if au.mutation.DefaultRegionCleared() {
		_spec.ClearField(awss3provider.FieldDefaultRegion, field.TypeString)
	}
	if value, ok := au.mutation.Valid(); ok {
		_spec.SetField(awss3provider.FieldValid, field.TypeBool, value)
	}
	if au.mutation.RepositoryCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: true,
			Table:   awss3provider.RepositoryTable,
			Columns: []string{awss3provider.RepositoryColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: repository.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := au.mutation.RepositoryIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: true,
			Table:   awss3provider.RepositoryTable,
			Columns: []string{awss3provider.RepositoryColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: repository.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	if n, err = sqlgraph.UpdateNodes(ctx, au.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{awss3provider.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return 0, err
	}
	return n, nil
}

// AWSS3ProviderUpdateOne is the builder for updating a single AWSS3Provider entity.
type AWSS3ProviderUpdateOne struct {
	config
	fields   []string
	hooks    []Hook
	mutation *AWSS3ProviderMutation
}

// SetBucketName sets the "bucketName" field.
func (auo *AWSS3ProviderUpdateOne) SetBucketName(s string) *AWSS3ProviderUpdateOne {
	auo.mutation.SetBucketName(s)
	return auo
}

// SetSecretAccessKey sets the "secretAccessKey" field.
func (auo *AWSS3ProviderUpdateOne) SetSecretAccessKey(s string) *AWSS3ProviderUpdateOne {
	auo.mutation.SetSecretAccessKey(s)
	return auo
}

// SetAccessKeyID sets the "accessKeyID" field.
func (auo *AWSS3ProviderUpdateOne) SetAccessKeyID(s string) *AWSS3ProviderUpdateOne {
	auo.mutation.SetAccessKeyID(s)
	return auo
}

// SetDefaultRegion sets the "defaultRegion" field.
func (auo *AWSS3ProviderUpdateOne) SetDefaultRegion(s string) *AWSS3ProviderUpdateOne {
	auo.mutation.SetDefaultRegion(s)
	return auo
}

// SetNillableDefaultRegion sets the "defaultRegion" field if the given value is not nil.
func (auo *AWSS3ProviderUpdateOne) SetNillableDefaultRegion(s *string) *AWSS3ProviderUpdateOne {
	if s != nil {
		auo.SetDefaultRegion(*s)
	}
	return auo
}

// ClearDefaultRegion clears the value of the "defaultRegion" field.
func (auo *AWSS3ProviderUpdateOne) ClearDefaultRegion() *AWSS3ProviderUpdateOne {
	auo.mutation.ClearDefaultRegion()
	return auo
}

// SetValid sets the "valid" field.
func (auo *AWSS3ProviderUpdateOne) SetValid(b bool) *AWSS3ProviderUpdateOne {
	auo.mutation.SetValid(b)
	return auo
}

// SetNillableValid sets the "valid" field if the given value is not nil.
func (auo *AWSS3ProviderUpdateOne) SetNillableValid(b *bool) *AWSS3ProviderUpdateOne {
	if b != nil {
		auo.SetValid(*b)
	}
	return auo
}

// SetRepositoryID sets the "repository" edge to the Repository entity by ID.
func (auo *AWSS3ProviderUpdateOne) SetRepositoryID(id int) *AWSS3ProviderUpdateOne {
	auo.mutation.SetRepositoryID(id)
	return auo
}

// SetRepository sets the "repository" edge to the Repository entity.
func (auo *AWSS3ProviderUpdateOne) SetRepository(r *Repository) *AWSS3ProviderUpdateOne {
	return auo.SetRepositoryID(r.ID)
}

// Mutation returns the AWSS3ProviderMutation object of the builder.
func (auo *AWSS3ProviderUpdateOne) Mutation() *AWSS3ProviderMutation {
	return auo.mutation
}

// ClearRepository clears the "repository" edge to the Repository entity.
func (auo *AWSS3ProviderUpdateOne) ClearRepository() *AWSS3ProviderUpdateOne {
	auo.mutation.ClearRepository()
	return auo
}

// Select allows selecting one or more fields (columns) of the returned entity.
// The default is selecting all fields defined in the entity schema.
func (auo *AWSS3ProviderUpdateOne) Select(field string, fields ...string) *AWSS3ProviderUpdateOne {
	auo.fields = append([]string{field}, fields...)
	return auo
}

// Save executes the query and returns the updated AWSS3Provider entity.
func (auo *AWSS3ProviderUpdateOne) Save(ctx context.Context) (*AWSS3Provider, error) {
	var (
		err  error
		node *AWSS3Provider
	)
	if len(auo.hooks) == 0 {
		if err = auo.check(); err != nil {
			return nil, err
		}
		node, err = auo.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*AWSS3ProviderMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = auo.check(); err != nil {
				return nil, err
			}
			auo.mutation = mutation
			node, err = auo.sqlSave(ctx)
			mutation.done = true
			return node, err
		})
		for i := len(auo.hooks) - 1; i >= 0; i-- {
			if auo.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = auo.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, auo.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*AWSS3Provider)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from AWSS3ProviderMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX is like Save, but panics if an error occurs.
func (auo *AWSS3ProviderUpdateOne) SaveX(ctx context.Context) *AWSS3Provider {
	node, err := auo.Save(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// Exec executes the query on the entity.
func (auo *AWSS3ProviderUpdateOne) Exec(ctx context.Context) error {
	_, err := auo.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (auo *AWSS3ProviderUpdateOne) ExecX(ctx context.Context) {
	if err := auo.Exec(ctx); err != nil {
		panic(err)
	}
}

// check runs all checks and user-defined validators on the builder.
func (auo *AWSS3ProviderUpdateOne) check() error {
	if v, ok := auo.mutation.BucketName(); ok {
		if err := awss3provider.BucketNameValidator(v); err != nil {
			return &ValidationError{Name: "bucketName", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.bucketName": %w`, err)}
		}
	}
	if v, ok := auo.mutation.SecretAccessKey(); ok {
		if err := awss3provider.SecretAccessKeyValidator(v); err != nil {
			return &ValidationError{Name: "secretAccessKey", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.secretAccessKey": %w`, err)}
		}
	}
	if v, ok := auo.mutation.AccessKeyID(); ok {
		if err := awss3provider.AccessKeyIDValidator(v); err != nil {
			return &ValidationError{Name: "accessKeyID", err: fmt.Errorf(`ent: validator failed for field "AWSS3Provider.accessKeyID": %w`, err)}
		}
	}
	if _, ok := auo.mutation.RepositoryID(); auo.mutation.RepositoryCleared() && !ok {
		return errors.New(`ent: clearing a required unique edge "AWSS3Provider.repository"`)
	}
	return nil
}

func (auo *AWSS3ProviderUpdateOne) sqlSave(ctx context.Context) (_node *AWSS3Provider, err error) {
	_spec := &sqlgraph.UpdateSpec{
		Node: &sqlgraph.NodeSpec{
			Table:   awss3provider.Table,
			Columns: awss3provider.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: awss3provider.FieldID,
			},
		},
	}
	id, ok := auo.mutation.ID()
	if !ok {
		return nil, &ValidationError{Name: "id", err: errors.New(`ent: missing "AWSS3Provider.id" for update`)}
	}
	_spec.Node.ID.Value = id
	if fields := auo.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, awss3provider.FieldID)
		for _, f := range fields {
			if !awss3provider.ValidColumn(f) {
				return nil, &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
			}
			if f != awss3provider.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, f)
			}
		}
	}
	if ps := auo.mutation.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if value, ok := auo.mutation.BucketName(); ok {
		_spec.SetField(awss3provider.FieldBucketName, field.TypeString, value)
	}
	if value, ok := auo.mutation.SecretAccessKey(); ok {
		_spec.SetField(awss3provider.FieldSecretAccessKey, field.TypeString, value)
	}
	if value, ok := auo.mutation.AccessKeyID(); ok {
		_spec.SetField(awss3provider.FieldAccessKeyID, field.TypeString, value)
	}
	if value, ok := auo.mutation.DefaultRegion(); ok {
		_spec.SetField(awss3provider.FieldDefaultRegion, field.TypeString, value)
	}
	if auo.mutation.DefaultRegionCleared() {
		_spec.ClearField(awss3provider.FieldDefaultRegion, field.TypeString)
	}
	if value, ok := auo.mutation.Valid(); ok {
		_spec.SetField(awss3provider.FieldValid, field.TypeBool, value)
	}
	if auo.mutation.RepositoryCleared() {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: true,
			Table:   awss3provider.RepositoryTable,
			Columns: []string{awss3provider.RepositoryColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: repository.FieldID,
				},
			},
		}
		_spec.Edges.Clear = append(_spec.Edges.Clear, edge)
	}
	if nodes := auo.mutation.RepositoryIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: true,
			Table:   awss3provider.RepositoryTable,
			Columns: []string{awss3provider.RepositoryColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: repository.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_spec.Edges.Add = append(_spec.Edges.Add, edge)
	}
	_node = &AWSS3Provider{config: auo.config}
	_spec.Assign = _node.assignValues
	_spec.ScanValues = _node.scanValues
	if err = sqlgraph.UpdateNode(ctx, auo.driver, _spec); err != nil {
		if _, ok := err.(*sqlgraph.NotFoundError); ok {
			err = &NotFoundError{awss3provider.Label}
		} else if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	return _node, nil
}
