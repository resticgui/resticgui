// Code generated by ent, DO NOT EDIT.

package user

const (
	// Label holds the string label denoting the user type in the database.
	Label = "user"
	// FieldID holds the string denoting the id field in the database.
	FieldID = "id"
	// FieldUsername holds the string denoting the username field in the database.
	FieldUsername = "username"
	// FieldEmail holds the string denoting the email field in the database.
	FieldEmail = "email"
	// FieldPassword holds the string denoting the password field in the database.
	FieldPassword = "password"
	// FieldLastConnectionDate holds the string denoting the lastconnectiondate field in the database.
	FieldLastConnectionDate = "last_connection_date"
	// FieldPasswordLastSet holds the string denoting the passwordlastset field in the database.
	FieldPasswordLastSet = "password_last_set"
	// FieldPasswordFailCount holds the string denoting the passwordfailcount field in the database.
	FieldPasswordFailCount = "password_fail_count"
	// FieldMfa holds the string denoting the mfa field in the database.
	FieldMfa = "mfa"
	// Table holds the table name of the user in the database.
	Table = "users"
)

// Columns holds all SQL columns for user fields.
var Columns = []string{
	FieldID,
	FieldUsername,
	FieldEmail,
	FieldPassword,
	FieldLastConnectionDate,
	FieldPasswordLastSet,
	FieldPasswordFailCount,
	FieldMfa,
}

// ValidColumn reports if the column name is valid (part of the table columns).
func ValidColumn(column string) bool {
	for i := range Columns {
		if column == Columns[i] {
			return true
		}
	}
	return false
}

var (
	// DefaultPasswordFailCount holds the default value on creation for the "PasswordFailCount" field.
	DefaultPasswordFailCount int
	// DefaultMfa holds the default value on creation for the "mfa" field.
	DefaultMfa bool
)
