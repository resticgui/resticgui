// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"fmt"
	"math"

	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/resticgui/resticgui/ent/awss3provider"
	"gitlab.com/resticgui/resticgui/ent/predicate"
	"gitlab.com/resticgui/resticgui/ent/repository"
)

// AWSS3ProviderQuery is the builder for querying AWSS3Provider entities.
type AWSS3ProviderQuery struct {
	config
	limit          *int
	offset         *int
	unique         *bool
	order          []OrderFunc
	fields         []string
	predicates     []predicate.AWSS3Provider
	withRepository *RepositoryQuery
	withFKs        bool
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Where adds a new predicate for the AWSS3ProviderQuery builder.
func (aq *AWSS3ProviderQuery) Where(ps ...predicate.AWSS3Provider) *AWSS3ProviderQuery {
	aq.predicates = append(aq.predicates, ps...)
	return aq
}

// Limit adds a limit step to the query.
func (aq *AWSS3ProviderQuery) Limit(limit int) *AWSS3ProviderQuery {
	aq.limit = &limit
	return aq
}

// Offset adds an offset step to the query.
func (aq *AWSS3ProviderQuery) Offset(offset int) *AWSS3ProviderQuery {
	aq.offset = &offset
	return aq
}

// Unique configures the query builder to filter duplicate records on query.
// By default, unique is set to true, and can be disabled using this method.
func (aq *AWSS3ProviderQuery) Unique(unique bool) *AWSS3ProviderQuery {
	aq.unique = &unique
	return aq
}

// Order adds an order step to the query.
func (aq *AWSS3ProviderQuery) Order(o ...OrderFunc) *AWSS3ProviderQuery {
	aq.order = append(aq.order, o...)
	return aq
}

// QueryRepository chains the current query on the "repository" edge.
func (aq *AWSS3ProviderQuery) QueryRepository() *RepositoryQuery {
	query := &RepositoryQuery{config: aq.config}
	query.path = func(ctx context.Context) (fromU *sql.Selector, err error) {
		if err := aq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		selector := aq.sqlQuery(ctx)
		if err := selector.Err(); err != nil {
			return nil, err
		}
		step := sqlgraph.NewStep(
			sqlgraph.From(awss3provider.Table, awss3provider.FieldID, selector),
			sqlgraph.To(repository.Table, repository.FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, awss3provider.RepositoryTable, awss3provider.RepositoryColumn),
		)
		fromU = sqlgraph.SetNeighbors(aq.driver.Dialect(), step)
		return fromU, nil
	}
	return query
}

// First returns the first AWSS3Provider entity from the query.
// Returns a *NotFoundError when no AWSS3Provider was found.
func (aq *AWSS3ProviderQuery) First(ctx context.Context) (*AWSS3Provider, error) {
	nodes, err := aq.Limit(1).All(ctx)
	if err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nil, &NotFoundError{awss3provider.Label}
	}
	return nodes[0], nil
}

// FirstX is like First, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) FirstX(ctx context.Context) *AWSS3Provider {
	node, err := aq.First(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return node
}

// FirstID returns the first AWSS3Provider ID from the query.
// Returns a *NotFoundError when no AWSS3Provider ID was found.
func (aq *AWSS3ProviderQuery) FirstID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = aq.Limit(1).IDs(ctx); err != nil {
		return
	}
	if len(ids) == 0 {
		err = &NotFoundError{awss3provider.Label}
		return
	}
	return ids[0], nil
}

// FirstIDX is like FirstID, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) FirstIDX(ctx context.Context) int {
	id, err := aq.FirstID(ctx)
	if err != nil && !IsNotFound(err) {
		panic(err)
	}
	return id
}

// Only returns a single AWSS3Provider entity found by the query, ensuring it only returns one.
// Returns a *NotSingularError when more than one AWSS3Provider entity is found.
// Returns a *NotFoundError when no AWSS3Provider entities are found.
func (aq *AWSS3ProviderQuery) Only(ctx context.Context) (*AWSS3Provider, error) {
	nodes, err := aq.Limit(2).All(ctx)
	if err != nil {
		return nil, err
	}
	switch len(nodes) {
	case 1:
		return nodes[0], nil
	case 0:
		return nil, &NotFoundError{awss3provider.Label}
	default:
		return nil, &NotSingularError{awss3provider.Label}
	}
}

// OnlyX is like Only, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) OnlyX(ctx context.Context) *AWSS3Provider {
	node, err := aq.Only(ctx)
	if err != nil {
		panic(err)
	}
	return node
}

// OnlyID is like Only, but returns the only AWSS3Provider ID in the query.
// Returns a *NotSingularError when more than one AWSS3Provider ID is found.
// Returns a *NotFoundError when no entities are found.
func (aq *AWSS3ProviderQuery) OnlyID(ctx context.Context) (id int, err error) {
	var ids []int
	if ids, err = aq.Limit(2).IDs(ctx); err != nil {
		return
	}
	switch len(ids) {
	case 1:
		id = ids[0]
	case 0:
		err = &NotFoundError{awss3provider.Label}
	default:
		err = &NotSingularError{awss3provider.Label}
	}
	return
}

// OnlyIDX is like OnlyID, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) OnlyIDX(ctx context.Context) int {
	id, err := aq.OnlyID(ctx)
	if err != nil {
		panic(err)
	}
	return id
}

// All executes the query and returns a list of AWSS3Providers.
func (aq *AWSS3ProviderQuery) All(ctx context.Context) ([]*AWSS3Provider, error) {
	if err := aq.prepareQuery(ctx); err != nil {
		return nil, err
	}
	return aq.sqlAll(ctx)
}

// AllX is like All, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) AllX(ctx context.Context) []*AWSS3Provider {
	nodes, err := aq.All(ctx)
	if err != nil {
		panic(err)
	}
	return nodes
}

// IDs executes the query and returns a list of AWSS3Provider IDs.
func (aq *AWSS3ProviderQuery) IDs(ctx context.Context) ([]int, error) {
	var ids []int
	if err := aq.Select(awss3provider.FieldID).Scan(ctx, &ids); err != nil {
		return nil, err
	}
	return ids, nil
}

// IDsX is like IDs, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) IDsX(ctx context.Context) []int {
	ids, err := aq.IDs(ctx)
	if err != nil {
		panic(err)
	}
	return ids
}

// Count returns the count of the given query.
func (aq *AWSS3ProviderQuery) Count(ctx context.Context) (int, error) {
	if err := aq.prepareQuery(ctx); err != nil {
		return 0, err
	}
	return aq.sqlCount(ctx)
}

// CountX is like Count, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) CountX(ctx context.Context) int {
	count, err := aq.Count(ctx)
	if err != nil {
		panic(err)
	}
	return count
}

// Exist returns true if the query has elements in the graph.
func (aq *AWSS3ProviderQuery) Exist(ctx context.Context) (bool, error) {
	if err := aq.prepareQuery(ctx); err != nil {
		return false, err
	}
	return aq.sqlExist(ctx)
}

// ExistX is like Exist, but panics if an error occurs.
func (aq *AWSS3ProviderQuery) ExistX(ctx context.Context) bool {
	exist, err := aq.Exist(ctx)
	if err != nil {
		panic(err)
	}
	return exist
}

// Clone returns a duplicate of the AWSS3ProviderQuery builder, including all associated steps. It can be
// used to prepare common query builders and use them differently after the clone is made.
func (aq *AWSS3ProviderQuery) Clone() *AWSS3ProviderQuery {
	if aq == nil {
		return nil
	}
	return &AWSS3ProviderQuery{
		config:         aq.config,
		limit:          aq.limit,
		offset:         aq.offset,
		order:          append([]OrderFunc{}, aq.order...),
		predicates:     append([]predicate.AWSS3Provider{}, aq.predicates...),
		withRepository: aq.withRepository.Clone(),
		// clone intermediate query.
		sql:    aq.sql.Clone(),
		path:   aq.path,
		unique: aq.unique,
	}
}

// WithRepository tells the query-builder to eager-load the nodes that are connected to
// the "repository" edge. The optional arguments are used to configure the query builder of the edge.
func (aq *AWSS3ProviderQuery) WithRepository(opts ...func(*RepositoryQuery)) *AWSS3ProviderQuery {
	query := &RepositoryQuery{config: aq.config}
	for _, opt := range opts {
		opt(query)
	}
	aq.withRepository = query
	return aq
}

// GroupBy is used to group vertices by one or more fields/columns.
// It is often used with aggregate functions, like: count, max, mean, min, sum.
//
// Example:
//
//	var v []struct {
//		BucketName string `json:"bucketName,omitempty"`
//		Count int `json:"count,omitempty"`
//	}
//
//	client.AWSS3Provider.Query().
//		GroupBy(awss3provider.FieldBucketName).
//		Aggregate(ent.Count()).
//		Scan(ctx, &v)
func (aq *AWSS3ProviderQuery) GroupBy(field string, fields ...string) *AWSS3ProviderGroupBy {
	grbuild := &AWSS3ProviderGroupBy{config: aq.config}
	grbuild.fields = append([]string{field}, fields...)
	grbuild.path = func(ctx context.Context) (prev *sql.Selector, err error) {
		if err := aq.prepareQuery(ctx); err != nil {
			return nil, err
		}
		return aq.sqlQuery(ctx), nil
	}
	grbuild.label = awss3provider.Label
	grbuild.flds, grbuild.scan = &grbuild.fields, grbuild.Scan
	return grbuild
}

// Select allows the selection one or more fields/columns for the given query,
// instead of selecting all fields in the entity.
//
// Example:
//
//	var v []struct {
//		BucketName string `json:"bucketName,omitempty"`
//	}
//
//	client.AWSS3Provider.Query().
//		Select(awss3provider.FieldBucketName).
//		Scan(ctx, &v)
func (aq *AWSS3ProviderQuery) Select(fields ...string) *AWSS3ProviderSelect {
	aq.fields = append(aq.fields, fields...)
	selbuild := &AWSS3ProviderSelect{AWSS3ProviderQuery: aq}
	selbuild.label = awss3provider.Label
	selbuild.flds, selbuild.scan = &aq.fields, selbuild.Scan
	return selbuild
}

// Aggregate returns a AWSS3ProviderSelect configured with the given aggregations.
func (aq *AWSS3ProviderQuery) Aggregate(fns ...AggregateFunc) *AWSS3ProviderSelect {
	return aq.Select().Aggregate(fns...)
}

func (aq *AWSS3ProviderQuery) prepareQuery(ctx context.Context) error {
	for _, f := range aq.fields {
		if !awss3provider.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("ent: invalid field %q for query", f)}
		}
	}
	if aq.path != nil {
		prev, err := aq.path(ctx)
		if err != nil {
			return err
		}
		aq.sql = prev
	}
	return nil
}

func (aq *AWSS3ProviderQuery) sqlAll(ctx context.Context, hooks ...queryHook) ([]*AWSS3Provider, error) {
	var (
		nodes       = []*AWSS3Provider{}
		withFKs     = aq.withFKs
		_spec       = aq.querySpec()
		loadedTypes = [1]bool{
			aq.withRepository != nil,
		}
	)
	if aq.withRepository != nil {
		withFKs = true
	}
	if withFKs {
		_spec.Node.Columns = append(_spec.Node.Columns, awss3provider.ForeignKeys...)
	}
	_spec.ScanValues = func(columns []string) ([]any, error) {
		return (*AWSS3Provider).scanValues(nil, columns)
	}
	_spec.Assign = func(columns []string, values []any) error {
		node := &AWSS3Provider{config: aq.config}
		nodes = append(nodes, node)
		node.Edges.loadedTypes = loadedTypes
		return node.assignValues(columns, values)
	}
	for i := range hooks {
		hooks[i](ctx, _spec)
	}
	if err := sqlgraph.QueryNodes(ctx, aq.driver, _spec); err != nil {
		return nil, err
	}
	if len(nodes) == 0 {
		return nodes, nil
	}
	if query := aq.withRepository; query != nil {
		if err := aq.loadRepository(ctx, query, nodes, nil,
			func(n *AWSS3Provider, e *Repository) { n.Edges.Repository = e }); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

func (aq *AWSS3ProviderQuery) loadRepository(ctx context.Context, query *RepositoryQuery, nodes []*AWSS3Provider, init func(*AWSS3Provider), assign func(*AWSS3Provider, *Repository)) error {
	ids := make([]int, 0, len(nodes))
	nodeids := make(map[int][]*AWSS3Provider)
	for i := range nodes {
		if nodes[i].repository_awss3provider == nil {
			continue
		}
		fk := *nodes[i].repository_awss3provider
		if _, ok := nodeids[fk]; !ok {
			ids = append(ids, fk)
		}
		nodeids[fk] = append(nodeids[fk], nodes[i])
	}
	query.Where(repository.IDIn(ids...))
	neighbors, err := query.All(ctx)
	if err != nil {
		return err
	}
	for _, n := range neighbors {
		nodes, ok := nodeids[n.ID]
		if !ok {
			return fmt.Errorf(`unexpected foreign-key "repository_awss3provider" returned %v`, n.ID)
		}
		for i := range nodes {
			assign(nodes[i], n)
		}
	}
	return nil
}

func (aq *AWSS3ProviderQuery) sqlCount(ctx context.Context) (int, error) {
	_spec := aq.querySpec()
	_spec.Node.Columns = aq.fields
	if len(aq.fields) > 0 {
		_spec.Unique = aq.unique != nil && *aq.unique
	}
	return sqlgraph.CountNodes(ctx, aq.driver, _spec)
}

func (aq *AWSS3ProviderQuery) sqlExist(ctx context.Context) (bool, error) {
	switch _, err := aq.FirstID(ctx); {
	case IsNotFound(err):
		return false, nil
	case err != nil:
		return false, fmt.Errorf("ent: check existence: %w", err)
	default:
		return true, nil
	}
}

func (aq *AWSS3ProviderQuery) querySpec() *sqlgraph.QuerySpec {
	_spec := &sqlgraph.QuerySpec{
		Node: &sqlgraph.NodeSpec{
			Table:   awss3provider.Table,
			Columns: awss3provider.Columns,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: awss3provider.FieldID,
			},
		},
		From:   aq.sql,
		Unique: true,
	}
	if unique := aq.unique; unique != nil {
		_spec.Unique = *unique
	}
	if fields := aq.fields; len(fields) > 0 {
		_spec.Node.Columns = make([]string, 0, len(fields))
		_spec.Node.Columns = append(_spec.Node.Columns, awss3provider.FieldID)
		for i := range fields {
			if fields[i] != awss3provider.FieldID {
				_spec.Node.Columns = append(_spec.Node.Columns, fields[i])
			}
		}
	}
	if ps := aq.predicates; len(ps) > 0 {
		_spec.Predicate = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	if limit := aq.limit; limit != nil {
		_spec.Limit = *limit
	}
	if offset := aq.offset; offset != nil {
		_spec.Offset = *offset
	}
	if ps := aq.order; len(ps) > 0 {
		_spec.Order = func(selector *sql.Selector) {
			for i := range ps {
				ps[i](selector)
			}
		}
	}
	return _spec
}

func (aq *AWSS3ProviderQuery) sqlQuery(ctx context.Context) *sql.Selector {
	builder := sql.Dialect(aq.driver.Dialect())
	t1 := builder.Table(awss3provider.Table)
	columns := aq.fields
	if len(columns) == 0 {
		columns = awss3provider.Columns
	}
	selector := builder.Select(t1.Columns(columns...)...).From(t1)
	if aq.sql != nil {
		selector = aq.sql
		selector.Select(selector.Columns(columns...)...)
	}
	if aq.unique != nil && *aq.unique {
		selector.Distinct()
	}
	for _, p := range aq.predicates {
		p(selector)
	}
	for _, p := range aq.order {
		p(selector)
	}
	if offset := aq.offset; offset != nil {
		// limit is mandatory for offset clause. We start
		// with default value, and override it below if needed.
		selector.Offset(*offset).Limit(math.MaxInt32)
	}
	if limit := aq.limit; limit != nil {
		selector.Limit(*limit)
	}
	return selector
}

// AWSS3ProviderGroupBy is the group-by builder for AWSS3Provider entities.
type AWSS3ProviderGroupBy struct {
	config
	selector
	fields []string
	fns    []AggregateFunc
	// intermediate query (i.e. traversal path).
	sql  *sql.Selector
	path func(context.Context) (*sql.Selector, error)
}

// Aggregate adds the given aggregation functions to the group-by query.
func (agb *AWSS3ProviderGroupBy) Aggregate(fns ...AggregateFunc) *AWSS3ProviderGroupBy {
	agb.fns = append(agb.fns, fns...)
	return agb
}

// Scan applies the group-by query and scans the result into the given value.
func (agb *AWSS3ProviderGroupBy) Scan(ctx context.Context, v any) error {
	query, err := agb.path(ctx)
	if err != nil {
		return err
	}
	agb.sql = query
	return agb.sqlScan(ctx, v)
}

func (agb *AWSS3ProviderGroupBy) sqlScan(ctx context.Context, v any) error {
	for _, f := range agb.fields {
		if !awss3provider.ValidColumn(f) {
			return &ValidationError{Name: f, err: fmt.Errorf("invalid field %q for group-by", f)}
		}
	}
	selector := agb.sqlQuery()
	if err := selector.Err(); err != nil {
		return err
	}
	rows := &sql.Rows{}
	query, args := selector.Query()
	if err := agb.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}

func (agb *AWSS3ProviderGroupBy) sqlQuery() *sql.Selector {
	selector := agb.sql.Select()
	aggregation := make([]string, 0, len(agb.fns))
	for _, fn := range agb.fns {
		aggregation = append(aggregation, fn(selector))
	}
	if len(selector.SelectedColumns()) == 0 {
		columns := make([]string, 0, len(agb.fields)+len(agb.fns))
		for _, f := range agb.fields {
			columns = append(columns, selector.C(f))
		}
		columns = append(columns, aggregation...)
		selector.Select(columns...)
	}
	return selector.GroupBy(selector.Columns(agb.fields...)...)
}

// AWSS3ProviderSelect is the builder for selecting fields of AWSS3Provider entities.
type AWSS3ProviderSelect struct {
	*AWSS3ProviderQuery
	selector
	// intermediate query (i.e. traversal path).
	sql *sql.Selector
}

// Aggregate adds the given aggregation functions to the selector query.
func (as *AWSS3ProviderSelect) Aggregate(fns ...AggregateFunc) *AWSS3ProviderSelect {
	as.fns = append(as.fns, fns...)
	return as
}

// Scan applies the selector query and scans the result into the given value.
func (as *AWSS3ProviderSelect) Scan(ctx context.Context, v any) error {
	if err := as.prepareQuery(ctx); err != nil {
		return err
	}
	as.sql = as.AWSS3ProviderQuery.sqlQuery(ctx)
	return as.sqlScan(ctx, v)
}

func (as *AWSS3ProviderSelect) sqlScan(ctx context.Context, v any) error {
	aggregation := make([]string, 0, len(as.fns))
	for _, fn := range as.fns {
		aggregation = append(aggregation, fn(as.sql))
	}
	switch n := len(*as.selector.flds); {
	case n == 0 && len(aggregation) > 0:
		as.sql.Select(aggregation...)
	case n != 0 && len(aggregation) > 0:
		as.sql.AppendSelect(aggregation...)
	}
	rows := &sql.Rows{}
	query, args := as.sql.Query()
	if err := as.driver.Query(ctx, query, args, rows); err != nil {
		return err
	}
	defer rows.Close()
	return sql.ScanSlice(rows, v)
}
