// Code generated by ent, DO NOT EDIT.

package ent

import (
	"context"
	"errors"
	"fmt"

	"entgo.io/ent/dialect/sql/sqlgraph"
	"entgo.io/ent/schema/field"
	"gitlab.com/resticgui/resticgui/ent/backupschedule"
	"gitlab.com/resticgui/resticgui/ent/repository"
)

// BackupScheduleCreate is the builder for creating a BackupSchedule entity.
type BackupScheduleCreate struct {
	config
	mutation *BackupScheduleMutation
	hooks    []Hook
}

// SetChoice sets the "choice" field.
func (bsc *BackupScheduleCreate) SetChoice(b backupschedule.Choice) *BackupScheduleCreate {
	bsc.mutation.SetChoice(b)
	return bsc
}

// SetNillableChoice sets the "choice" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableChoice(b *backupschedule.Choice) *BackupScheduleCreate {
	if b != nil {
		bsc.SetChoice(*b)
	}
	return bsc
}

// SetEveryDay sets the "everyDay" field.
func (bsc *BackupScheduleCreate) SetEveryDay(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryDay(b)
	return bsc
}

// SetNillableEveryDay sets the "everyDay" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryDay(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryDay(*b)
	}
	return bsc
}

// SetEveryWeek sets the "everyWeek" field.
func (bsc *BackupScheduleCreate) SetEveryWeek(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryWeek(b)
	return bsc
}

// SetNillableEveryWeek sets the "everyWeek" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryWeek(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryWeek(*b)
	}
	return bsc
}

// SetEveryMonth sets the "everyMonth" field.
func (bsc *BackupScheduleCreate) SetEveryMonth(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryMonth(b)
	return bsc
}

// SetNillableEveryMonth sets the "everyMonth" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryMonth(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryMonth(*b)
	}
	return bsc
}

// SetEveryMonday sets the "everyMonday" field.
func (bsc *BackupScheduleCreate) SetEveryMonday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryMonday(b)
	return bsc
}

// SetNillableEveryMonday sets the "everyMonday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryMonday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryMonday(*b)
	}
	return bsc
}

// SetEveryTuesday sets the "everyTuesday" field.
func (bsc *BackupScheduleCreate) SetEveryTuesday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryTuesday(b)
	return bsc
}

// SetNillableEveryTuesday sets the "everyTuesday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryTuesday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryTuesday(*b)
	}
	return bsc
}

// SetEveryWednesday sets the "everyWednesday" field.
func (bsc *BackupScheduleCreate) SetEveryWednesday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryWednesday(b)
	return bsc
}

// SetNillableEveryWednesday sets the "everyWednesday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryWednesday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryWednesday(*b)
	}
	return bsc
}

// SetEveryThursday sets the "everyThursday" field.
func (bsc *BackupScheduleCreate) SetEveryThursday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryThursday(b)
	return bsc
}

// SetNillableEveryThursday sets the "everyThursday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryThursday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryThursday(*b)
	}
	return bsc
}

// SetEveryFriday sets the "everyFriday" field.
func (bsc *BackupScheduleCreate) SetEveryFriday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEveryFriday(b)
	return bsc
}

// SetNillableEveryFriday sets the "everyFriday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEveryFriday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEveryFriday(*b)
	}
	return bsc
}

// SetEverySaturday sets the "everySaturday" field.
func (bsc *BackupScheduleCreate) SetEverySaturday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEverySaturday(b)
	return bsc
}

// SetNillableEverySaturday sets the "everySaturday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEverySaturday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEverySaturday(*b)
	}
	return bsc
}

// SetEverySunday sets the "everySunday" field.
func (bsc *BackupScheduleCreate) SetEverySunday(b bool) *BackupScheduleCreate {
	bsc.mutation.SetEverySunday(b)
	return bsc
}

// SetNillableEverySunday sets the "everySunday" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableEverySunday(b *bool) *BackupScheduleCreate {
	if b != nil {
		bsc.SetEverySunday(*b)
	}
	return bsc
}

// SetHour sets the "hour" field.
func (bsc *BackupScheduleCreate) SetHour(s string) *BackupScheduleCreate {
	bsc.mutation.SetHour(s)
	return bsc
}

// SetNillableHour sets the "hour" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableHour(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetHour(*s)
	}
	return bsc
}

// SetDay sets the "day" field.
func (bsc *BackupScheduleCreate) SetDay(s string) *BackupScheduleCreate {
	bsc.mutation.SetDay(s)
	return bsc
}

// SetNillableDay sets the "day" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableDay(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetDay(*s)
	}
	return bsc
}

// SetInterval sets the "interval" field.
func (bsc *BackupScheduleCreate) SetInterval(s string) *BackupScheduleCreate {
	bsc.mutation.SetInterval(s)
	return bsc
}

// SetNillableInterval sets the "interval" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableInterval(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetInterval(*s)
	}
	return bsc
}

// SetCronSyntax sets the "cronSyntax" field.
func (bsc *BackupScheduleCreate) SetCronSyntax(s string) *BackupScheduleCreate {
	bsc.mutation.SetCronSyntax(s)
	return bsc
}

// SetNillableCronSyntax sets the "cronSyntax" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableCronSyntax(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetCronSyntax(*s)
	}
	return bsc
}

// SetCronID sets the "cronID" field.
func (bsc *BackupScheduleCreate) SetCronID(i int) *BackupScheduleCreate {
	bsc.mutation.SetCronID(i)
	return bsc
}

// SetNillableCronID sets the "cronID" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableCronID(i *int) *BackupScheduleCreate {
	if i != nil {
		bsc.SetCronID(*i)
	}
	return bsc
}

// SetNextRun sets the "nextRun" field.
func (bsc *BackupScheduleCreate) SetNextRun(s string) *BackupScheduleCreate {
	bsc.mutation.SetNextRun(s)
	return bsc
}

// SetNillableNextRun sets the "nextRun" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableNextRun(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetNextRun(*s)
	}
	return bsc
}

// SetLastRun sets the "lastRun" field.
func (bsc *BackupScheduleCreate) SetLastRun(s string) *BackupScheduleCreate {
	bsc.mutation.SetLastRun(s)
	return bsc
}

// SetNillableLastRun sets the "lastRun" field if the given value is not nil.
func (bsc *BackupScheduleCreate) SetNillableLastRun(s *string) *BackupScheduleCreate {
	if s != nil {
		bsc.SetLastRun(*s)
	}
	return bsc
}

// SetRepositoryID sets the "repository" edge to the Repository entity by ID.
func (bsc *BackupScheduleCreate) SetRepositoryID(id int) *BackupScheduleCreate {
	bsc.mutation.SetRepositoryID(id)
	return bsc
}

// SetRepository sets the "repository" edge to the Repository entity.
func (bsc *BackupScheduleCreate) SetRepository(r *Repository) *BackupScheduleCreate {
	return bsc.SetRepositoryID(r.ID)
}

// Mutation returns the BackupScheduleMutation object of the builder.
func (bsc *BackupScheduleCreate) Mutation() *BackupScheduleMutation {
	return bsc.mutation
}

// Save creates the BackupSchedule in the database.
func (bsc *BackupScheduleCreate) Save(ctx context.Context) (*BackupSchedule, error) {
	var (
		err  error
		node *BackupSchedule
	)
	bsc.defaults()
	if len(bsc.hooks) == 0 {
		if err = bsc.check(); err != nil {
			return nil, err
		}
		node, err = bsc.sqlSave(ctx)
	} else {
		var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
			mutation, ok := m.(*BackupScheduleMutation)
			if !ok {
				return nil, fmt.Errorf("unexpected mutation type %T", m)
			}
			if err = bsc.check(); err != nil {
				return nil, err
			}
			bsc.mutation = mutation
			if node, err = bsc.sqlSave(ctx); err != nil {
				return nil, err
			}
			mutation.id = &node.ID
			mutation.done = true
			return node, err
		})
		for i := len(bsc.hooks) - 1; i >= 0; i-- {
			if bsc.hooks[i] == nil {
				return nil, fmt.Errorf("ent: uninitialized hook (forgotten import ent/runtime?)")
			}
			mut = bsc.hooks[i](mut)
		}
		v, err := mut.Mutate(ctx, bsc.mutation)
		if err != nil {
			return nil, err
		}
		nv, ok := v.(*BackupSchedule)
		if !ok {
			return nil, fmt.Errorf("unexpected node type %T returned from BackupScheduleMutation", v)
		}
		node = nv
	}
	return node, err
}

// SaveX calls Save and panics if Save returns an error.
func (bsc *BackupScheduleCreate) SaveX(ctx context.Context) *BackupSchedule {
	v, err := bsc.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (bsc *BackupScheduleCreate) Exec(ctx context.Context) error {
	_, err := bsc.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (bsc *BackupScheduleCreate) ExecX(ctx context.Context) {
	if err := bsc.Exec(ctx); err != nil {
		panic(err)
	}
}

// defaults sets the default values of the builder before save.
func (bsc *BackupScheduleCreate) defaults() {
	if _, ok := bsc.mutation.Choice(); !ok {
		v := backupschedule.DefaultChoice
		bsc.mutation.SetChoice(v)
	}
	if _, ok := bsc.mutation.EveryDay(); !ok {
		v := backupschedule.DefaultEveryDay
		bsc.mutation.SetEveryDay(v)
	}
	if _, ok := bsc.mutation.EveryWeek(); !ok {
		v := backupschedule.DefaultEveryWeek
		bsc.mutation.SetEveryWeek(v)
	}
	if _, ok := bsc.mutation.EveryMonth(); !ok {
		v := backupschedule.DefaultEveryMonth
		bsc.mutation.SetEveryMonth(v)
	}
	if _, ok := bsc.mutation.EveryMonday(); !ok {
		v := backupschedule.DefaultEveryMonday
		bsc.mutation.SetEveryMonday(v)
	}
	if _, ok := bsc.mutation.EveryTuesday(); !ok {
		v := backupschedule.DefaultEveryTuesday
		bsc.mutation.SetEveryTuesday(v)
	}
	if _, ok := bsc.mutation.EveryWednesday(); !ok {
		v := backupschedule.DefaultEveryWednesday
		bsc.mutation.SetEveryWednesday(v)
	}
	if _, ok := bsc.mutation.EveryThursday(); !ok {
		v := backupschedule.DefaultEveryThursday
		bsc.mutation.SetEveryThursday(v)
	}
	if _, ok := bsc.mutation.EveryFriday(); !ok {
		v := backupschedule.DefaultEveryFriday
		bsc.mutation.SetEveryFriday(v)
	}
	if _, ok := bsc.mutation.EverySaturday(); !ok {
		v := backupschedule.DefaultEverySaturday
		bsc.mutation.SetEverySaturday(v)
	}
	if _, ok := bsc.mutation.EverySunday(); !ok {
		v := backupschedule.DefaultEverySunday
		bsc.mutation.SetEverySunday(v)
	}
	if _, ok := bsc.mutation.Hour(); !ok {
		v := backupschedule.DefaultHour
		bsc.mutation.SetHour(v)
	}
	if _, ok := bsc.mutation.Day(); !ok {
		v := backupschedule.DefaultDay
		bsc.mutation.SetDay(v)
	}
	if _, ok := bsc.mutation.Interval(); !ok {
		v := backupschedule.DefaultInterval
		bsc.mutation.SetInterval(v)
	}
	if _, ok := bsc.mutation.CronSyntax(); !ok {
		v := backupschedule.DefaultCronSyntax
		bsc.mutation.SetCronSyntax(v)
	}
	if _, ok := bsc.mutation.NextRun(); !ok {
		v := backupschedule.DefaultNextRun
		bsc.mutation.SetNextRun(v)
	}
	if _, ok := bsc.mutation.LastRun(); !ok {
		v := backupschedule.DefaultLastRun
		bsc.mutation.SetLastRun(v)
	}
}

// check runs all checks and user-defined validators on the builder.
func (bsc *BackupScheduleCreate) check() error {
	if _, ok := bsc.mutation.Choice(); !ok {
		return &ValidationError{Name: "choice", err: errors.New(`ent: missing required field "BackupSchedule.choice"`)}
	}
	if v, ok := bsc.mutation.Choice(); ok {
		if err := backupschedule.ChoiceValidator(v); err != nil {
			return &ValidationError{Name: "choice", err: fmt.Errorf(`ent: validator failed for field "BackupSchedule.choice": %w`, err)}
		}
	}
	if _, ok := bsc.mutation.EveryDay(); !ok {
		return &ValidationError{Name: "everyDay", err: errors.New(`ent: missing required field "BackupSchedule.everyDay"`)}
	}
	if _, ok := bsc.mutation.EveryWeek(); !ok {
		return &ValidationError{Name: "everyWeek", err: errors.New(`ent: missing required field "BackupSchedule.everyWeek"`)}
	}
	if _, ok := bsc.mutation.EveryMonth(); !ok {
		return &ValidationError{Name: "everyMonth", err: errors.New(`ent: missing required field "BackupSchedule.everyMonth"`)}
	}
	if _, ok := bsc.mutation.EveryMonday(); !ok {
		return &ValidationError{Name: "everyMonday", err: errors.New(`ent: missing required field "BackupSchedule.everyMonday"`)}
	}
	if _, ok := bsc.mutation.EveryTuesday(); !ok {
		return &ValidationError{Name: "everyTuesday", err: errors.New(`ent: missing required field "BackupSchedule.everyTuesday"`)}
	}
	if _, ok := bsc.mutation.EveryWednesday(); !ok {
		return &ValidationError{Name: "everyWednesday", err: errors.New(`ent: missing required field "BackupSchedule.everyWednesday"`)}
	}
	if _, ok := bsc.mutation.EveryThursday(); !ok {
		return &ValidationError{Name: "everyThursday", err: errors.New(`ent: missing required field "BackupSchedule.everyThursday"`)}
	}
	if _, ok := bsc.mutation.EveryFriday(); !ok {
		return &ValidationError{Name: "everyFriday", err: errors.New(`ent: missing required field "BackupSchedule.everyFriday"`)}
	}
	if _, ok := bsc.mutation.EverySaturday(); !ok {
		return &ValidationError{Name: "everySaturday", err: errors.New(`ent: missing required field "BackupSchedule.everySaturday"`)}
	}
	if _, ok := bsc.mutation.EverySunday(); !ok {
		return &ValidationError{Name: "everySunday", err: errors.New(`ent: missing required field "BackupSchedule.everySunday"`)}
	}
	if _, ok := bsc.mutation.RepositoryID(); !ok {
		return &ValidationError{Name: "repository", err: errors.New(`ent: missing required edge "BackupSchedule.repository"`)}
	}
	return nil
}

func (bsc *BackupScheduleCreate) sqlSave(ctx context.Context) (*BackupSchedule, error) {
	_node, _spec := bsc.createSpec()
	if err := sqlgraph.CreateNode(ctx, bsc.driver, _spec); err != nil {
		if sqlgraph.IsConstraintError(err) {
			err = &ConstraintError{msg: err.Error(), wrap: err}
		}
		return nil, err
	}
	id := _spec.ID.Value.(int64)
	_node.ID = int(id)
	return _node, nil
}

func (bsc *BackupScheduleCreate) createSpec() (*BackupSchedule, *sqlgraph.CreateSpec) {
	var (
		_node = &BackupSchedule{config: bsc.config}
		_spec = &sqlgraph.CreateSpec{
			Table: backupschedule.Table,
			ID: &sqlgraph.FieldSpec{
				Type:   field.TypeInt,
				Column: backupschedule.FieldID,
			},
		}
	)
	if value, ok := bsc.mutation.Choice(); ok {
		_spec.SetField(backupschedule.FieldChoice, field.TypeEnum, value)
		_node.Choice = value
	}
	if value, ok := bsc.mutation.EveryDay(); ok {
		_spec.SetField(backupschedule.FieldEveryDay, field.TypeBool, value)
		_node.EveryDay = value
	}
	if value, ok := bsc.mutation.EveryWeek(); ok {
		_spec.SetField(backupschedule.FieldEveryWeek, field.TypeBool, value)
		_node.EveryWeek = value
	}
	if value, ok := bsc.mutation.EveryMonth(); ok {
		_spec.SetField(backupschedule.FieldEveryMonth, field.TypeBool, value)
		_node.EveryMonth = value
	}
	if value, ok := bsc.mutation.EveryMonday(); ok {
		_spec.SetField(backupschedule.FieldEveryMonday, field.TypeBool, value)
		_node.EveryMonday = value
	}
	if value, ok := bsc.mutation.EveryTuesday(); ok {
		_spec.SetField(backupschedule.FieldEveryTuesday, field.TypeBool, value)
		_node.EveryTuesday = value
	}
	if value, ok := bsc.mutation.EveryWednesday(); ok {
		_spec.SetField(backupschedule.FieldEveryWednesday, field.TypeBool, value)
		_node.EveryWednesday = value
	}
	if value, ok := bsc.mutation.EveryThursday(); ok {
		_spec.SetField(backupschedule.FieldEveryThursday, field.TypeBool, value)
		_node.EveryThursday = value
	}
	if value, ok := bsc.mutation.EveryFriday(); ok {
		_spec.SetField(backupschedule.FieldEveryFriday, field.TypeBool, value)
		_node.EveryFriday = value
	}
	if value, ok := bsc.mutation.EverySaturday(); ok {
		_spec.SetField(backupschedule.FieldEverySaturday, field.TypeBool, value)
		_node.EverySaturday = value
	}
	if value, ok := bsc.mutation.EverySunday(); ok {
		_spec.SetField(backupschedule.FieldEverySunday, field.TypeBool, value)
		_node.EverySunday = value
	}
	if value, ok := bsc.mutation.Hour(); ok {
		_spec.SetField(backupschedule.FieldHour, field.TypeString, value)
		_node.Hour = value
	}
	if value, ok := bsc.mutation.Day(); ok {
		_spec.SetField(backupschedule.FieldDay, field.TypeString, value)
		_node.Day = value
	}
	if value, ok := bsc.mutation.Interval(); ok {
		_spec.SetField(backupschedule.FieldInterval, field.TypeString, value)
		_node.Interval = value
	}
	if value, ok := bsc.mutation.CronSyntax(); ok {
		_spec.SetField(backupschedule.FieldCronSyntax, field.TypeString, value)
		_node.CronSyntax = value
	}
	if value, ok := bsc.mutation.CronID(); ok {
		_spec.SetField(backupschedule.FieldCronID, field.TypeInt, value)
		_node.CronID = &value
	}
	if value, ok := bsc.mutation.NextRun(); ok {
		_spec.SetField(backupschedule.FieldNextRun, field.TypeString, value)
		_node.NextRun = value
	}
	if value, ok := bsc.mutation.LastRun(); ok {
		_spec.SetField(backupschedule.FieldLastRun, field.TypeString, value)
		_node.LastRun = value
	}
	if nodes := bsc.mutation.RepositoryIDs(); len(nodes) > 0 {
		edge := &sqlgraph.EdgeSpec{
			Rel:     sqlgraph.O2O,
			Inverse: true,
			Table:   backupschedule.RepositoryTable,
			Columns: []string{backupschedule.RepositoryColumn},
			Bidi:    false,
			Target: &sqlgraph.EdgeTarget{
				IDSpec: &sqlgraph.FieldSpec{
					Type:   field.TypeInt,
					Column: repository.FieldID,
				},
			},
		}
		for _, k := range nodes {
			edge.Target.Nodes = append(edge.Target.Nodes, k)
		}
		_node.repository_backup_schedule = &nodes[0]
		_spec.Edges = append(_spec.Edges, edge)
	}
	return _node, _spec
}

// BackupScheduleCreateBulk is the builder for creating many BackupSchedule entities in bulk.
type BackupScheduleCreateBulk struct {
	config
	builders []*BackupScheduleCreate
}

// Save creates the BackupSchedule entities in the database.
func (bscb *BackupScheduleCreateBulk) Save(ctx context.Context) ([]*BackupSchedule, error) {
	specs := make([]*sqlgraph.CreateSpec, len(bscb.builders))
	nodes := make([]*BackupSchedule, len(bscb.builders))
	mutators := make([]Mutator, len(bscb.builders))
	for i := range bscb.builders {
		func(i int, root context.Context) {
			builder := bscb.builders[i]
			builder.defaults()
			var mut Mutator = MutateFunc(func(ctx context.Context, m Mutation) (Value, error) {
				mutation, ok := m.(*BackupScheduleMutation)
				if !ok {
					return nil, fmt.Errorf("unexpected mutation type %T", m)
				}
				if err := builder.check(); err != nil {
					return nil, err
				}
				builder.mutation = mutation
				nodes[i], specs[i] = builder.createSpec()
				var err error
				if i < len(mutators)-1 {
					_, err = mutators[i+1].Mutate(root, bscb.builders[i+1].mutation)
				} else {
					spec := &sqlgraph.BatchCreateSpec{Nodes: specs}
					// Invoke the actual operation on the latest mutation in the chain.
					if err = sqlgraph.BatchCreate(ctx, bscb.driver, spec); err != nil {
						if sqlgraph.IsConstraintError(err) {
							err = &ConstraintError{msg: err.Error(), wrap: err}
						}
					}
				}
				if err != nil {
					return nil, err
				}
				mutation.id = &nodes[i].ID
				if specs[i].ID.Value != nil {
					id := specs[i].ID.Value.(int64)
					nodes[i].ID = int(id)
				}
				mutation.done = true
				return nodes[i], nil
			})
			for i := len(builder.hooks) - 1; i >= 0; i-- {
				mut = builder.hooks[i](mut)
			}
			mutators[i] = mut
		}(i, ctx)
	}
	if len(mutators) > 0 {
		if _, err := mutators[0].Mutate(ctx, bscb.builders[0].mutation); err != nil {
			return nil, err
		}
	}
	return nodes, nil
}

// SaveX is like Save, but panics if an error occurs.
func (bscb *BackupScheduleCreateBulk) SaveX(ctx context.Context) []*BackupSchedule {
	v, err := bscb.Save(ctx)
	if err != nil {
		panic(err)
	}
	return v
}

// Exec executes the query.
func (bscb *BackupScheduleCreateBulk) Exec(ctx context.Context) error {
	_, err := bscb.Save(ctx)
	return err
}

// ExecX is like Exec, but panics if an error occurs.
func (bscb *BackupScheduleCreateBulk) ExecX(ctx context.Context) {
	if err := bscb.Exec(ctx); err != nil {
		panic(err)
	}
}
