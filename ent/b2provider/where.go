// Code generated by ent, DO NOT EDIT.

package b2provider

import (
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"gitlab.com/resticgui/resticgui/ent/predicate"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		v := make([]any, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		v := make([]any, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// BucketName applies equality check predicate on the "bucketName" field. It's identical to BucketNameEQ.
func BucketName(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldBucketName), v))
	})
}

// AccountKey applies equality check predicate on the "accountKey" field. It's identical to AccountKeyEQ.
func AccountKey(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAccountKey), v))
	})
}

// AccountID applies equality check predicate on the "accountID" field. It's identical to AccountIDEQ.
func AccountID(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAccountID), v))
	})
}

// RepoName applies equality check predicate on the "repoName" field. It's identical to RepoNameEQ.
func RepoName(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldRepoName), v))
	})
}

// Valid applies equality check predicate on the "valid" field. It's identical to ValidEQ.
func Valid(v bool) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldValid), v))
	})
}

// BucketNameEQ applies the EQ predicate on the "bucketName" field.
func BucketNameEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldBucketName), v))
	})
}

// BucketNameNEQ applies the NEQ predicate on the "bucketName" field.
func BucketNameNEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldBucketName), v))
	})
}

// BucketNameIn applies the In predicate on the "bucketName" field.
func BucketNameIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldBucketName), v...))
	})
}

// BucketNameNotIn applies the NotIn predicate on the "bucketName" field.
func BucketNameNotIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldBucketName), v...))
	})
}

// BucketNameGT applies the GT predicate on the "bucketName" field.
func BucketNameGT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldBucketName), v))
	})
}

// BucketNameGTE applies the GTE predicate on the "bucketName" field.
func BucketNameGTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldBucketName), v))
	})
}

// BucketNameLT applies the LT predicate on the "bucketName" field.
func BucketNameLT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldBucketName), v))
	})
}

// BucketNameLTE applies the LTE predicate on the "bucketName" field.
func BucketNameLTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldBucketName), v))
	})
}

// BucketNameContains applies the Contains predicate on the "bucketName" field.
func BucketNameContains(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldBucketName), v))
	})
}

// BucketNameHasPrefix applies the HasPrefix predicate on the "bucketName" field.
func BucketNameHasPrefix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldBucketName), v))
	})
}

// BucketNameHasSuffix applies the HasSuffix predicate on the "bucketName" field.
func BucketNameHasSuffix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldBucketName), v))
	})
}

// BucketNameEqualFold applies the EqualFold predicate on the "bucketName" field.
func BucketNameEqualFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldBucketName), v))
	})
}

// BucketNameContainsFold applies the ContainsFold predicate on the "bucketName" field.
func BucketNameContainsFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldBucketName), v))
	})
}

// AccountKeyEQ applies the EQ predicate on the "accountKey" field.
func AccountKeyEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAccountKey), v))
	})
}

// AccountKeyNEQ applies the NEQ predicate on the "accountKey" field.
func AccountKeyNEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldAccountKey), v))
	})
}

// AccountKeyIn applies the In predicate on the "accountKey" field.
func AccountKeyIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldAccountKey), v...))
	})
}

// AccountKeyNotIn applies the NotIn predicate on the "accountKey" field.
func AccountKeyNotIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldAccountKey), v...))
	})
}

// AccountKeyGT applies the GT predicate on the "accountKey" field.
func AccountKeyGT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldAccountKey), v))
	})
}

// AccountKeyGTE applies the GTE predicate on the "accountKey" field.
func AccountKeyGTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldAccountKey), v))
	})
}

// AccountKeyLT applies the LT predicate on the "accountKey" field.
func AccountKeyLT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldAccountKey), v))
	})
}

// AccountKeyLTE applies the LTE predicate on the "accountKey" field.
func AccountKeyLTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldAccountKey), v))
	})
}

// AccountKeyContains applies the Contains predicate on the "accountKey" field.
func AccountKeyContains(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldAccountKey), v))
	})
}

// AccountKeyHasPrefix applies the HasPrefix predicate on the "accountKey" field.
func AccountKeyHasPrefix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldAccountKey), v))
	})
}

// AccountKeyHasSuffix applies the HasSuffix predicate on the "accountKey" field.
func AccountKeyHasSuffix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldAccountKey), v))
	})
}

// AccountKeyEqualFold applies the EqualFold predicate on the "accountKey" field.
func AccountKeyEqualFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldAccountKey), v))
	})
}

// AccountKeyContainsFold applies the ContainsFold predicate on the "accountKey" field.
func AccountKeyContainsFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldAccountKey), v))
	})
}

// AccountIDEQ applies the EQ predicate on the "accountID" field.
func AccountIDEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldAccountID), v))
	})
}

// AccountIDNEQ applies the NEQ predicate on the "accountID" field.
func AccountIDNEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldAccountID), v))
	})
}

// AccountIDIn applies the In predicate on the "accountID" field.
func AccountIDIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldAccountID), v...))
	})
}

// AccountIDNotIn applies the NotIn predicate on the "accountID" field.
func AccountIDNotIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldAccountID), v...))
	})
}

// AccountIDGT applies the GT predicate on the "accountID" field.
func AccountIDGT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldAccountID), v))
	})
}

// AccountIDGTE applies the GTE predicate on the "accountID" field.
func AccountIDGTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldAccountID), v))
	})
}

// AccountIDLT applies the LT predicate on the "accountID" field.
func AccountIDLT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldAccountID), v))
	})
}

// AccountIDLTE applies the LTE predicate on the "accountID" field.
func AccountIDLTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldAccountID), v))
	})
}

// AccountIDContains applies the Contains predicate on the "accountID" field.
func AccountIDContains(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldAccountID), v))
	})
}

// AccountIDHasPrefix applies the HasPrefix predicate on the "accountID" field.
func AccountIDHasPrefix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldAccountID), v))
	})
}

// AccountIDHasSuffix applies the HasSuffix predicate on the "accountID" field.
func AccountIDHasSuffix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldAccountID), v))
	})
}

// AccountIDEqualFold applies the EqualFold predicate on the "accountID" field.
func AccountIDEqualFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldAccountID), v))
	})
}

// AccountIDContainsFold applies the ContainsFold predicate on the "accountID" field.
func AccountIDContainsFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldAccountID), v))
	})
}

// RepoNameEQ applies the EQ predicate on the "repoName" field.
func RepoNameEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldRepoName), v))
	})
}

// RepoNameNEQ applies the NEQ predicate on the "repoName" field.
func RepoNameNEQ(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldRepoName), v))
	})
}

// RepoNameIn applies the In predicate on the "repoName" field.
func RepoNameIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldRepoName), v...))
	})
}

// RepoNameNotIn applies the NotIn predicate on the "repoName" field.
func RepoNameNotIn(vs ...string) predicate.B2Provider {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldRepoName), v...))
	})
}

// RepoNameGT applies the GT predicate on the "repoName" field.
func RepoNameGT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldRepoName), v))
	})
}

// RepoNameGTE applies the GTE predicate on the "repoName" field.
func RepoNameGTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldRepoName), v))
	})
}

// RepoNameLT applies the LT predicate on the "repoName" field.
func RepoNameLT(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldRepoName), v))
	})
}

// RepoNameLTE applies the LTE predicate on the "repoName" field.
func RepoNameLTE(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldRepoName), v))
	})
}

// RepoNameContains applies the Contains predicate on the "repoName" field.
func RepoNameContains(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldRepoName), v))
	})
}

// RepoNameHasPrefix applies the HasPrefix predicate on the "repoName" field.
func RepoNameHasPrefix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldRepoName), v))
	})
}

// RepoNameHasSuffix applies the HasSuffix predicate on the "repoName" field.
func RepoNameHasSuffix(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldRepoName), v))
	})
}

// RepoNameEqualFold applies the EqualFold predicate on the "repoName" field.
func RepoNameEqualFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldRepoName), v))
	})
}

// RepoNameContainsFold applies the ContainsFold predicate on the "repoName" field.
func RepoNameContainsFold(v string) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldRepoName), v))
	})
}

// ValidEQ applies the EQ predicate on the "valid" field.
func ValidEQ(v bool) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldValid), v))
	})
}

// ValidNEQ applies the NEQ predicate on the "valid" field.
func ValidNEQ(v bool) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldValid), v))
	})
}

// HasRepository applies the HasEdge predicate on the "repository" edge.
func HasRepository() predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasRepositoryWith applies the HasEdge predicate on the "repository" edge with a given conditions (other predicates).
func HasRepositoryWith(preds ...predicate.Repository) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.B2Provider) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.B2Provider) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.B2Provider) predicate.B2Provider {
	return predicate.B2Provider(func(s *sql.Selector) {
		p(s.Not())
	})
}
