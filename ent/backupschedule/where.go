// Code generated by ent, DO NOT EDIT.

package backupschedule

import (
	"entgo.io/ent/dialect/sql"
	"entgo.io/ent/dialect/sql/sqlgraph"
	"gitlab.com/resticgui/resticgui/ent/predicate"
)

// ID filters vertices based on their ID field.
func ID(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDEQ applies the EQ predicate on the ID field.
func IDEQ(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldID), id))
	})
}

// IDNEQ applies the NEQ predicate on the ID field.
func IDNEQ(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldID), id))
	})
}

// IDIn applies the In predicate on the ID field.
func IDIn(ids ...int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		v := make([]any, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.In(s.C(FieldID), v...))
	})
}

// IDNotIn applies the NotIn predicate on the ID field.
func IDNotIn(ids ...int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		v := make([]any, len(ids))
		for i := range v {
			v[i] = ids[i]
		}
		s.Where(sql.NotIn(s.C(FieldID), v...))
	})
}

// IDGT applies the GT predicate on the ID field.
func IDGT(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldID), id))
	})
}

// IDGTE applies the GTE predicate on the ID field.
func IDGTE(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldID), id))
	})
}

// IDLT applies the LT predicate on the ID field.
func IDLT(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldID), id))
	})
}

// IDLTE applies the LTE predicate on the ID field.
func IDLTE(id int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldID), id))
	})
}

// EveryDay applies equality check predicate on the "everyDay" field. It's identical to EveryDayEQ.
func EveryDay(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryDay), v))
	})
}

// EveryWeek applies equality check predicate on the "everyWeek" field. It's identical to EveryWeekEQ.
func EveryWeek(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryWeek), v))
	})
}

// EveryMonth applies equality check predicate on the "everyMonth" field. It's identical to EveryMonthEQ.
func EveryMonth(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryMonth), v))
	})
}

// EveryMonday applies equality check predicate on the "everyMonday" field. It's identical to EveryMondayEQ.
func EveryMonday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryMonday), v))
	})
}

// EveryTuesday applies equality check predicate on the "everyTuesday" field. It's identical to EveryTuesdayEQ.
func EveryTuesday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryTuesday), v))
	})
}

// EveryWednesday applies equality check predicate on the "everyWednesday" field. It's identical to EveryWednesdayEQ.
func EveryWednesday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryWednesday), v))
	})
}

// EveryThursday applies equality check predicate on the "everyThursday" field. It's identical to EveryThursdayEQ.
func EveryThursday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryThursday), v))
	})
}

// EveryFriday applies equality check predicate on the "everyFriday" field. It's identical to EveryFridayEQ.
func EveryFriday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryFriday), v))
	})
}

// EverySaturday applies equality check predicate on the "everySaturday" field. It's identical to EverySaturdayEQ.
func EverySaturday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEverySaturday), v))
	})
}

// EverySunday applies equality check predicate on the "everySunday" field. It's identical to EverySundayEQ.
func EverySunday(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEverySunday), v))
	})
}

// Hour applies equality check predicate on the "hour" field. It's identical to HourEQ.
func Hour(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldHour), v))
	})
}

// Day applies equality check predicate on the "day" field. It's identical to DayEQ.
func Day(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldDay), v))
	})
}

// Interval applies equality check predicate on the "interval" field. It's identical to IntervalEQ.
func Interval(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldInterval), v))
	})
}

// CronSyntax applies equality check predicate on the "cronSyntax" field. It's identical to CronSyntaxEQ.
func CronSyntax(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCronSyntax), v))
	})
}

// CronID applies equality check predicate on the "cronID" field. It's identical to CronIDEQ.
func CronID(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCronID), v))
	})
}

// NextRun applies equality check predicate on the "nextRun" field. It's identical to NextRunEQ.
func NextRun(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldNextRun), v))
	})
}

// LastRun applies equality check predicate on the "lastRun" field. It's identical to LastRunEQ.
func LastRun(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldLastRun), v))
	})
}

// ChoiceEQ applies the EQ predicate on the "choice" field.
func ChoiceEQ(v Choice) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldChoice), v))
	})
}

// ChoiceNEQ applies the NEQ predicate on the "choice" field.
func ChoiceNEQ(v Choice) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldChoice), v))
	})
}

// ChoiceIn applies the In predicate on the "choice" field.
func ChoiceIn(vs ...Choice) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldChoice), v...))
	})
}

// ChoiceNotIn applies the NotIn predicate on the "choice" field.
func ChoiceNotIn(vs ...Choice) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldChoice), v...))
	})
}

// EveryDayEQ applies the EQ predicate on the "everyDay" field.
func EveryDayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryDay), v))
	})
}

// EveryDayNEQ applies the NEQ predicate on the "everyDay" field.
func EveryDayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryDay), v))
	})
}

// EveryWeekEQ applies the EQ predicate on the "everyWeek" field.
func EveryWeekEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryWeek), v))
	})
}

// EveryWeekNEQ applies the NEQ predicate on the "everyWeek" field.
func EveryWeekNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryWeek), v))
	})
}

// EveryMonthEQ applies the EQ predicate on the "everyMonth" field.
func EveryMonthEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryMonth), v))
	})
}

// EveryMonthNEQ applies the NEQ predicate on the "everyMonth" field.
func EveryMonthNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryMonth), v))
	})
}

// EveryMondayEQ applies the EQ predicate on the "everyMonday" field.
func EveryMondayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryMonday), v))
	})
}

// EveryMondayNEQ applies the NEQ predicate on the "everyMonday" field.
func EveryMondayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryMonday), v))
	})
}

// EveryTuesdayEQ applies the EQ predicate on the "everyTuesday" field.
func EveryTuesdayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryTuesday), v))
	})
}

// EveryTuesdayNEQ applies the NEQ predicate on the "everyTuesday" field.
func EveryTuesdayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryTuesday), v))
	})
}

// EveryWednesdayEQ applies the EQ predicate on the "everyWednesday" field.
func EveryWednesdayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryWednesday), v))
	})
}

// EveryWednesdayNEQ applies the NEQ predicate on the "everyWednesday" field.
func EveryWednesdayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryWednesday), v))
	})
}

// EveryThursdayEQ applies the EQ predicate on the "everyThursday" field.
func EveryThursdayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryThursday), v))
	})
}

// EveryThursdayNEQ applies the NEQ predicate on the "everyThursday" field.
func EveryThursdayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryThursday), v))
	})
}

// EveryFridayEQ applies the EQ predicate on the "everyFriday" field.
func EveryFridayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEveryFriday), v))
	})
}

// EveryFridayNEQ applies the NEQ predicate on the "everyFriday" field.
func EveryFridayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEveryFriday), v))
	})
}

// EverySaturdayEQ applies the EQ predicate on the "everySaturday" field.
func EverySaturdayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEverySaturday), v))
	})
}

// EverySaturdayNEQ applies the NEQ predicate on the "everySaturday" field.
func EverySaturdayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEverySaturday), v))
	})
}

// EverySundayEQ applies the EQ predicate on the "everySunday" field.
func EverySundayEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldEverySunday), v))
	})
}

// EverySundayNEQ applies the NEQ predicate on the "everySunday" field.
func EverySundayNEQ(v bool) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldEverySunday), v))
	})
}

// HourEQ applies the EQ predicate on the "hour" field.
func HourEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldHour), v))
	})
}

// HourNEQ applies the NEQ predicate on the "hour" field.
func HourNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldHour), v))
	})
}

// HourIn applies the In predicate on the "hour" field.
func HourIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldHour), v...))
	})
}

// HourNotIn applies the NotIn predicate on the "hour" field.
func HourNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldHour), v...))
	})
}

// HourGT applies the GT predicate on the "hour" field.
func HourGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldHour), v))
	})
}

// HourGTE applies the GTE predicate on the "hour" field.
func HourGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldHour), v))
	})
}

// HourLT applies the LT predicate on the "hour" field.
func HourLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldHour), v))
	})
}

// HourLTE applies the LTE predicate on the "hour" field.
func HourLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldHour), v))
	})
}

// HourContains applies the Contains predicate on the "hour" field.
func HourContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldHour), v))
	})
}

// HourHasPrefix applies the HasPrefix predicate on the "hour" field.
func HourHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldHour), v))
	})
}

// HourHasSuffix applies the HasSuffix predicate on the "hour" field.
func HourHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldHour), v))
	})
}

// HourIsNil applies the IsNil predicate on the "hour" field.
func HourIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldHour)))
	})
}

// HourNotNil applies the NotNil predicate on the "hour" field.
func HourNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldHour)))
	})
}

// HourEqualFold applies the EqualFold predicate on the "hour" field.
func HourEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldHour), v))
	})
}

// HourContainsFold applies the ContainsFold predicate on the "hour" field.
func HourContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldHour), v))
	})
}

// DayEQ applies the EQ predicate on the "day" field.
func DayEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldDay), v))
	})
}

// DayNEQ applies the NEQ predicate on the "day" field.
func DayNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldDay), v))
	})
}

// DayIn applies the In predicate on the "day" field.
func DayIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldDay), v...))
	})
}

// DayNotIn applies the NotIn predicate on the "day" field.
func DayNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldDay), v...))
	})
}

// DayGT applies the GT predicate on the "day" field.
func DayGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldDay), v))
	})
}

// DayGTE applies the GTE predicate on the "day" field.
func DayGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldDay), v))
	})
}

// DayLT applies the LT predicate on the "day" field.
func DayLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldDay), v))
	})
}

// DayLTE applies the LTE predicate on the "day" field.
func DayLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldDay), v))
	})
}

// DayContains applies the Contains predicate on the "day" field.
func DayContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldDay), v))
	})
}

// DayHasPrefix applies the HasPrefix predicate on the "day" field.
func DayHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldDay), v))
	})
}

// DayHasSuffix applies the HasSuffix predicate on the "day" field.
func DayHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldDay), v))
	})
}

// DayIsNil applies the IsNil predicate on the "day" field.
func DayIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldDay)))
	})
}

// DayNotNil applies the NotNil predicate on the "day" field.
func DayNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldDay)))
	})
}

// DayEqualFold applies the EqualFold predicate on the "day" field.
func DayEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldDay), v))
	})
}

// DayContainsFold applies the ContainsFold predicate on the "day" field.
func DayContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldDay), v))
	})
}

// IntervalEQ applies the EQ predicate on the "interval" field.
func IntervalEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldInterval), v))
	})
}

// IntervalNEQ applies the NEQ predicate on the "interval" field.
func IntervalNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldInterval), v))
	})
}

// IntervalIn applies the In predicate on the "interval" field.
func IntervalIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldInterval), v...))
	})
}

// IntervalNotIn applies the NotIn predicate on the "interval" field.
func IntervalNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldInterval), v...))
	})
}

// IntervalGT applies the GT predicate on the "interval" field.
func IntervalGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldInterval), v))
	})
}

// IntervalGTE applies the GTE predicate on the "interval" field.
func IntervalGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldInterval), v))
	})
}

// IntervalLT applies the LT predicate on the "interval" field.
func IntervalLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldInterval), v))
	})
}

// IntervalLTE applies the LTE predicate on the "interval" field.
func IntervalLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldInterval), v))
	})
}

// IntervalContains applies the Contains predicate on the "interval" field.
func IntervalContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldInterval), v))
	})
}

// IntervalHasPrefix applies the HasPrefix predicate on the "interval" field.
func IntervalHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldInterval), v))
	})
}

// IntervalHasSuffix applies the HasSuffix predicate on the "interval" field.
func IntervalHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldInterval), v))
	})
}

// IntervalIsNil applies the IsNil predicate on the "interval" field.
func IntervalIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldInterval)))
	})
}

// IntervalNotNil applies the NotNil predicate on the "interval" field.
func IntervalNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldInterval)))
	})
}

// IntervalEqualFold applies the EqualFold predicate on the "interval" field.
func IntervalEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldInterval), v))
	})
}

// IntervalContainsFold applies the ContainsFold predicate on the "interval" field.
func IntervalContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldInterval), v))
	})
}

// CronSyntaxEQ applies the EQ predicate on the "cronSyntax" field.
func CronSyntaxEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxNEQ applies the NEQ predicate on the "cronSyntax" field.
func CronSyntaxNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxIn applies the In predicate on the "cronSyntax" field.
func CronSyntaxIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldCronSyntax), v...))
	})
}

// CronSyntaxNotIn applies the NotIn predicate on the "cronSyntax" field.
func CronSyntaxNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldCronSyntax), v...))
	})
}

// CronSyntaxGT applies the GT predicate on the "cronSyntax" field.
func CronSyntaxGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxGTE applies the GTE predicate on the "cronSyntax" field.
func CronSyntaxGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxLT applies the LT predicate on the "cronSyntax" field.
func CronSyntaxLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxLTE applies the LTE predicate on the "cronSyntax" field.
func CronSyntaxLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxContains applies the Contains predicate on the "cronSyntax" field.
func CronSyntaxContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxHasPrefix applies the HasPrefix predicate on the "cronSyntax" field.
func CronSyntaxHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxHasSuffix applies the HasSuffix predicate on the "cronSyntax" field.
func CronSyntaxHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxIsNil applies the IsNil predicate on the "cronSyntax" field.
func CronSyntaxIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldCronSyntax)))
	})
}

// CronSyntaxNotNil applies the NotNil predicate on the "cronSyntax" field.
func CronSyntaxNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldCronSyntax)))
	})
}

// CronSyntaxEqualFold applies the EqualFold predicate on the "cronSyntax" field.
func CronSyntaxEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldCronSyntax), v))
	})
}

// CronSyntaxContainsFold applies the ContainsFold predicate on the "cronSyntax" field.
func CronSyntaxContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldCronSyntax), v))
	})
}

// CronIDEQ applies the EQ predicate on the "cronID" field.
func CronIDEQ(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldCronID), v))
	})
}

// CronIDNEQ applies the NEQ predicate on the "cronID" field.
func CronIDNEQ(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldCronID), v))
	})
}

// CronIDIn applies the In predicate on the "cronID" field.
func CronIDIn(vs ...int) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldCronID), v...))
	})
}

// CronIDNotIn applies the NotIn predicate on the "cronID" field.
func CronIDNotIn(vs ...int) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldCronID), v...))
	})
}

// CronIDGT applies the GT predicate on the "cronID" field.
func CronIDGT(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldCronID), v))
	})
}

// CronIDGTE applies the GTE predicate on the "cronID" field.
func CronIDGTE(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldCronID), v))
	})
}

// CronIDLT applies the LT predicate on the "cronID" field.
func CronIDLT(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldCronID), v))
	})
}

// CronIDLTE applies the LTE predicate on the "cronID" field.
func CronIDLTE(v int) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldCronID), v))
	})
}

// CronIDIsNil applies the IsNil predicate on the "cronID" field.
func CronIDIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldCronID)))
	})
}

// CronIDNotNil applies the NotNil predicate on the "cronID" field.
func CronIDNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldCronID)))
	})
}

// NextRunEQ applies the EQ predicate on the "nextRun" field.
func NextRunEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldNextRun), v))
	})
}

// NextRunNEQ applies the NEQ predicate on the "nextRun" field.
func NextRunNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldNextRun), v))
	})
}

// NextRunIn applies the In predicate on the "nextRun" field.
func NextRunIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldNextRun), v...))
	})
}

// NextRunNotIn applies the NotIn predicate on the "nextRun" field.
func NextRunNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldNextRun), v...))
	})
}

// NextRunGT applies the GT predicate on the "nextRun" field.
func NextRunGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldNextRun), v))
	})
}

// NextRunGTE applies the GTE predicate on the "nextRun" field.
func NextRunGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldNextRun), v))
	})
}

// NextRunLT applies the LT predicate on the "nextRun" field.
func NextRunLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldNextRun), v))
	})
}

// NextRunLTE applies the LTE predicate on the "nextRun" field.
func NextRunLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldNextRun), v))
	})
}

// NextRunContains applies the Contains predicate on the "nextRun" field.
func NextRunContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldNextRun), v))
	})
}

// NextRunHasPrefix applies the HasPrefix predicate on the "nextRun" field.
func NextRunHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldNextRun), v))
	})
}

// NextRunHasSuffix applies the HasSuffix predicate on the "nextRun" field.
func NextRunHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldNextRun), v))
	})
}

// NextRunIsNil applies the IsNil predicate on the "nextRun" field.
func NextRunIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldNextRun)))
	})
}

// NextRunNotNil applies the NotNil predicate on the "nextRun" field.
func NextRunNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldNextRun)))
	})
}

// NextRunEqualFold applies the EqualFold predicate on the "nextRun" field.
func NextRunEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldNextRun), v))
	})
}

// NextRunContainsFold applies the ContainsFold predicate on the "nextRun" field.
func NextRunContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldNextRun), v))
	})
}

// LastRunEQ applies the EQ predicate on the "lastRun" field.
func LastRunEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EQ(s.C(FieldLastRun), v))
	})
}

// LastRunNEQ applies the NEQ predicate on the "lastRun" field.
func LastRunNEQ(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NEQ(s.C(FieldLastRun), v))
	})
}

// LastRunIn applies the In predicate on the "lastRun" field.
func LastRunIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.In(s.C(FieldLastRun), v...))
	})
}

// LastRunNotIn applies the NotIn predicate on the "lastRun" field.
func LastRunNotIn(vs ...string) predicate.BackupSchedule {
	v := make([]any, len(vs))
	for i := range v {
		v[i] = vs[i]
	}
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotIn(s.C(FieldLastRun), v...))
	})
}

// LastRunGT applies the GT predicate on the "lastRun" field.
func LastRunGT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GT(s.C(FieldLastRun), v))
	})
}

// LastRunGTE applies the GTE predicate on the "lastRun" field.
func LastRunGTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.GTE(s.C(FieldLastRun), v))
	})
}

// LastRunLT applies the LT predicate on the "lastRun" field.
func LastRunLT(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LT(s.C(FieldLastRun), v))
	})
}

// LastRunLTE applies the LTE predicate on the "lastRun" field.
func LastRunLTE(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.LTE(s.C(FieldLastRun), v))
	})
}

// LastRunContains applies the Contains predicate on the "lastRun" field.
func LastRunContains(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.Contains(s.C(FieldLastRun), v))
	})
}

// LastRunHasPrefix applies the HasPrefix predicate on the "lastRun" field.
func LastRunHasPrefix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasPrefix(s.C(FieldLastRun), v))
	})
}

// LastRunHasSuffix applies the HasSuffix predicate on the "lastRun" field.
func LastRunHasSuffix(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.HasSuffix(s.C(FieldLastRun), v))
	})
}

// LastRunIsNil applies the IsNil predicate on the "lastRun" field.
func LastRunIsNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.IsNull(s.C(FieldLastRun)))
	})
}

// LastRunNotNil applies the NotNil predicate on the "lastRun" field.
func LastRunNotNil() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.NotNull(s.C(FieldLastRun)))
	})
}

// LastRunEqualFold applies the EqualFold predicate on the "lastRun" field.
func LastRunEqualFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.EqualFold(s.C(FieldLastRun), v))
	})
}

// LastRunContainsFold applies the ContainsFold predicate on the "lastRun" field.
func LastRunContainsFold(v string) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s.Where(sql.ContainsFold(s.C(FieldLastRun), v))
	})
}

// HasRepository applies the HasEdge predicate on the "repository" edge.
func HasRepository() predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighbors(s, step)
	})
}

// HasRepositoryWith applies the HasEdge predicate on the "repository" edge with a given conditions (other predicates).
func HasRepositoryWith(preds ...predicate.Repository) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		step := sqlgraph.NewStep(
			sqlgraph.From(Table, FieldID),
			sqlgraph.To(RepositoryInverseTable, FieldID),
			sqlgraph.Edge(sqlgraph.O2O, true, RepositoryTable, RepositoryColumn),
		)
		sqlgraph.HasNeighborsWith(s, step, func(s *sql.Selector) {
			for _, p := range preds {
				p(s)
			}
		})
	})
}

// And groups predicates with the AND operator between them.
func And(predicates ...predicate.BackupSchedule) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for _, p := range predicates {
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Or groups predicates with the OR operator between them.
func Or(predicates ...predicate.BackupSchedule) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		s1 := s.Clone().SetP(nil)
		for i, p := range predicates {
			if i > 0 {
				s1.Or()
			}
			p(s1)
		}
		s.Where(s1.P())
	})
}

// Not applies the not operator on the given predicate.
func Not(p predicate.BackupSchedule) predicate.BackupSchedule {
	return predicate.BackupSchedule(func(s *sql.Selector) {
		p(s.Not())
	})
}
