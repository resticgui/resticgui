package utils

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

const PasswordMinLength = 4

func TestConfig() {
	viper.SetConfigName("config")         // name of config file (without extension)
	viper.SetConfigType("yaml")           // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("../../testdata") // optionally look for config in the working directory
	err := viper.ReadInConfig()           // Find and read the config file
	if err != nil {                       // Handle errors reading the config file
		panic(log.Panic().Err(err))
	}
}

// // Clean database before test
// func CleanDatabaseTest(db *gorm.DB, t assert.TestingT, table string) {
// 	request := fmt.Sprintf("DELETE FROM %s", table)
// 	result := db.Exec(request)
// 	assert.Nil(t, result.Error)
// }

func IsValidInt(s string) bool {
	_, err := strconv.Atoi(s)
	value := (err == nil)
	if value {
		log.Debug().Msgf("%s is a valid int", s)
	} else {
		log.Info().Msgf("%s is not a valid int", s)
	}
	return value
}

func ReturnValidID(c *gin.Context) (value int, err error) {
	id := c.Param("id")
	value, err = strconv.Atoi(id)
	if err != nil {
		// TODO: add variable with error message
		// Currently not exported by package api
		log.Err(err).Msg("id not valid")
		c.IndentedJSON(http.StatusBadRequest, gin.H{"error": "id not valid"})
	}
	return value, err
}
