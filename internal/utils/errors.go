package utils

const ErrMsgRInvalid_Provider = "valid repository has failed on provider check"
const ErrMsgRInvalid_Password = "password empty or too short, repository cannot be valid"
const ErrMsgRInvalid_Schedule = "valid repository has failed on schedule check"
const ErrMsgREmpty_Paths = "valid repository has failed on path check"
