package Models

type Stat struct {
	TotalSize      int `json:"total_size"`
	TotalFileCount int `json:"total_file_count"`
}
