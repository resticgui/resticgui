package Models

type RepositoryPassword struct {
	// Password holds the value of the "password" field.
	Password string `json:"password,omitempty"`
}
