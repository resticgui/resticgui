package database

import (
	"context"

	"github.com/rs/zerolog/log"
	"gitlab.com/resticgui/resticgui/ent"
)

func CreateBackupPolicyDB(backupPolicy ent.BackupPolicy, repoID int, ctx context.Context) (b *ent.BackupPolicy, err error) {
	log.Debug().Msg("CreateBackupPolicyDB()")

	b, err = db.BackupPolicy.
		Create().
		SetKeepAll(backupPolicy.KeepAll).
		SetNbKeepDays(backupPolicy.NbKeepDays).
		SetNbKeepHours(backupPolicy.NbKeepHours).
		SetNbKeepLast(backupPolicy.NbKeepLast).
		SetNbKeepMonths(backupPolicy.NbKeepMonths).
		SetNbKeepWeeks(backupPolicy.NbKeepWeeks).
		SetRepositoryID(repoID).
		Save(ctx)
	if err != nil {
		log.Err(err).Msgf("error: %v", errCreate)
	}

	return b, err
}

func UpdateBackupPolicyDB(backupPolicy ent.BackupPolicy, id int, ctx context.Context) (b *ent.BackupPolicy, err error) {
	log.Debug().Msg("updateBackupPolicyDB()")

	b, err = db.BackupPolicy.
		UpdateOneID(id).
		SetKeepAll(backupPolicy.KeepAll).
		SetNbKeepDays(backupPolicy.NbKeepDays).
		SetNbKeepHours(backupPolicy.NbKeepHours).
		SetNbKeepLast(backupPolicy.NbKeepLast).
		SetNbKeepMonths(backupPolicy.NbKeepMonths).
		SetNbKeepWeeks(backupPolicy.NbKeepWeeks).
		Save(ctx)

	if err != nil {
		log.Err(err).Msgf("error: %v", errCreate)
	}
	return b, err
}
