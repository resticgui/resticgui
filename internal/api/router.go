package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/robfig/cron/v3"
	"github.com/rs/zerolog/log"
	swaggerfiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"gitlab.com/resticgui/resticgui/docs"
	"gitlab.com/resticgui/resticgui/internal/backup"
	"gitlab.com/resticgui/resticgui/internal/webui"
)

var cr *cron.Cron

const APIBasePath = "/api/v1/"
const APITitle = "ResticGUI API"
const APIVersion = "1.0"
const APIdescription = "ResticGUI API allows to create and manipulates repositories and backups which translate in restic commands under the hood"
const APIHost = "localhost:3001"

func SetupRouter() *gin.Engine {

	// Activate when going to production
	//gin.SetMode(gin.ReleaseMode)
	docs.SwaggerInfo.BasePath = APIBasePath
	docs.SwaggerInfo.Title = APITitle
	docs.SwaggerInfo.Version = APIVersion
	docs.SwaggerInfo.Description = APIdescription
	docs.SwaggerInfo.Host = APIHost
	// define router
	router := gin.Default()

	go func() {
		cr = cron.New()
		cr.Start()
		for _, value := range cr.Entries() {
			log.Info().Msgf("%v", value.ID)
		}
		backup.CreateAllScheduledBackup(cr)
	}()

	router.StaticFS("/ui", http.FileSystem(http.FS(webui.UI)))

	router.NoRoute(func(c *gin.Context) {
		c.Redirect(http.StatusMovedPermanently, "/ui")
	})

	router.POST(APIBasePath+"paths", GetPath)

	grpRepositories := router.Group(APIBasePath + "repositories")
	{
		grpRepositories.GET("", QueryRepositories)
		grpRepositories.GET(":id", GetRepository)
		grpRepositories.POST("", CreateRepository)
		grpRepositories.POST(":id/backupschedule", CreateBackupSchedule)
		grpRepositories.PATCH(":id/backupschedule", UpdateBackupSchedule)
		grpRepositories.POST(":id/pruneschedule", CreatePruneSchedule)
		grpRepositories.PATCH(":id/pruneschedule", UpdatePruneSchedule)
		grpRepositories.POST(":id/backuppolicy", CreateBackupPolicy)
		grpRepositories.PATCH(":id/backuppolicy", UpdateBackupPolicy)
		grpRepositories.POST(":id/provider", CreateProvider)
		grpRepositories.PATCH(":id/provider", UpdateProvider)
		grpRepositories.POST(":id/paths", CreatePaths)
		grpRepositories.PATCH(":id/paths", UpdatePaths)
		grpRepositories.PATCH(":id", EditRepository)
		grpRepositories.DELETE(":id", RemoveRepository)
		grpRepositories.GET(":id/logs", LogsRepository)
		grpRepositories.GET(":id/stats", StatsRepository)
		grpRepositories.GET(":id/check", CheckRepository)
		grpRepositories.GET(":id/snapshots", GetSnapshotsRepository)
		grpRepositories.DELETE(":id/snapshots/:snapshotID/forget", ForgetSnapshotRepository)
		grpRepositories.GET(":id/snapshots/:snapshotID/restore", RestoreSnapshotRepository)
		grpRepositories.GET(":id/snapshots/:snapshotID/ls", ListFilesSnapshotRepository)
		grpRepositories.GET(":id/snapshots/:snapshotID/search", SearchFilesSnapshotRepository)
		grpRepositories.GET(":id/snapshots/:snapshotID/download", DownloadFilesSnapshotRepository)
		grpRepositories.GET(":id/valid", ValidRepository)
	}

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerfiles.Handler))

	return router
}
