ARG NODEVERSION=18.15.0-alpine
ARG BASEDOCKERIMAGE=ubuntu:jammy-20230301
ARG GOVERSION=1.20.5-alpine
ARG TARGETARCH=amd64

FROM docker.io/node:${NODEVERSION} as BuilderWeb

COPY front/ /front

WORKDIR /front

RUN npm ci

RUN npm run build

ARG BASEDOCKERIMAGE

FROM docker.io/${BASEDOCKERIMAGE} as BuilderRestic

ARG TARGETARCH
ARG RESTICVERSION=0.15.1
ARG RESTICURL=https://github.com/restic/restic/releases/download/v${RESTICVERSION}/restic_${RESTICVERSION}_linux_${TARGETARCH}.bz2
ARG BUILDDIR=./build

RUN apt-get update && apt-get install -y \
    curl \
    openssl \
    bzip2

RUN mkdir -p ${BUILDDIR}
RUN curl \
    --request GET -sL \
    --url ${RESTICURL} \
    --output ${BUILDDIR}/restic.bz2

RUN bzip2 -d ${BUILDDIR}/restic.bz2

RUN	cp "${BUILDDIR}/restic" /restic

ARG GOVERSION
ARG TARGETARCH

FROM docker.io/golang:${GOVERSION} as BuilderResticGui

RUN apk update && apk add musl-dev gcc

ARG BUILDDIR=./build

RUN mkdir -p ${BUILDDIR}

COPY . ${BUILDDIR}

COPY --from=BuilderWeb /front/build ${BUILDDIR}/internal/webui/ui

WORKDIR ${BUILDDIR}

RUN cd cmd/resticgui && GOARCH=${TARGETARCH} CGO_ENABLED=1 go build -o /resticgui -ldflags '-w -s -extldflags "-static"'

ARG BASEDOCKERIMAGE

FROM docker.io/${BASEDOCKERIMAGE}

ARG RESTICGUIVERSION="0.2.0"

LABEL "hanotaux.mathieu.vendor"="MHX"
LABEL version=${RESTICGUIVERSION}
LABEL description="This image contains Restic and Resticgui webserver"

EXPOSE 3030

WORKDIR /opt

ENV GIN_MODE=release

RUN apt-get update && apt-get install -y \
    curl \
    && rm -rf /var/lib/apt/lists/*

COPY cmd/resticgui/config-docker.yaml ./config.yaml

COPY --from=BuilderRestic --chmod=700 /restic /usr/bin/restic
COPY --from=BuilderResticGui /resticgui resticgui

RUN mkdir /opt/db

CMD ["/opt/resticgui"]
